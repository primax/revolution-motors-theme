<div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fa fa-sliders"></i>Powertrain</a> </h4>
                                                    </div>
                                                    <div id="collapse2" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <h3 class="contant-hed">Engine/Transmissions</h3>
                                                            <div class="option-view"> 
                                                                <img src="/wp-content/themes/Avada/images/options/430HP-Man.jpg"> 
                                                            </div> 
                                                            
        <div class="dataRow">
            <h3 class="heading-options">Modern Performance</h3>
            <div class="optBox">
                <label title="430 Horsepower Manual" for="430HP-Man" class="label_container">430 Horsepower LS3 Engine with 6 Speed Manual Transmission <strong>$42,678.00</strong> 
                <input type="radio" id="430HP-Man" checked="checked" name="Engine-and-Trans" class="custom_radio" data-elementName="430 Horsepower LS3 Engine with 6 Speed Manual Transmission" data-pricetag="42678">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label title="430 Horsepower Automatic" for="430HP-Auto" class="label_container">430 Horsepower LS3 Engine with 4L65E Automatic Transmission <strong>$42,972.30</strong> 
                <input type="radio" id="430HP-Auto" name="Engine-and-Trans" class="custom_radio" data-pricetag="42972.30" data-elementName="430 Horsepower LS3 Engine with 4L65E Automatic Transmission">
                <span class="checkmark"></span> </label>
            </div>
        </div>
        
        <div class="dataRow">
            <h3 class="heading-options">High Performance </h3>
            <div class="optBox">
                <label title="556 Horsepower Manual" for="556HP-Man" class="label_container">556 Horsepower LSA Supercharged Engine with 6 Speed Manual Transmission <strong>$50,471.55</strong> 
                <input type="radio" id="556HP-Man" name="Engine-and-Trans" class="custom_radio" data-pricetag="50471.55" data-elementName="556 Horsepower LSA Supercharged Engine with 6 Speed Manual Transmission">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label title="556 Horsepower Automatic" for="556HP-Auto" class="label_container">556 Horsepower LSA Supercharged Engine with 4L75E Automatic Transmission <strong>$50,876.55</strong> 
                <input type="radio" id="556HP-Auto" name="Engine-and-Trans" class="custom_radio" data-pricetag="50876.55" data-elementName="556 Horsepower LSA Supercharged Engine with 4L75E Automatic Transmission">
                <span class="checkmark"></span> </label>
            </div>
        </div>
        
        <div class="dataRow">
            <h3 class="heading-options">Competition Power</h3>
            <div class="optBox">
                <label title="815 Horsepower Automatic" for="815HP-Auto" class="label_container">815 Horsepower Edelbrock LTI Supercharged Engine with 8L90E Automatic Transmission <strong>$63,392.40</strong> 
                <input type="radio" id="815HP-Auto" name="Engine-and-Trans" class="custom_radio" data-pricetag="63392.40" data-elementName="815 Horsepower Edelbrock LTI Supercharged Engine with 8L90E Automatic Transmission">
                <span class="checkmark"></span> </label>
            </div> 
        </div> 
                                                                    
                                                                    
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fa fa-bullseye"></i>Brakes</a> </h4>
                                                    </div>
                                                    <div id="collapse3" class="panel-collapse collapse">
                                                        <div class="panel-body"> 
                                                        <div class="option-view">
															<img src="/wp-content/themes/Avada/images/options/DB-4piston.jpg"> 
                                                        </div> 
                                                        <h3 class="heading-options">Select Brake type</h3>
		<div class="dataRow"> 
            <div class="optBox">
                <label title="Disc Brake 4 Piston" for="DB-4piston" class="label_container">Front & Rear Power Disc Brake System with four wheel Wilwood 4 Piston Calipers and Drilled & Slotted Rotors <small>$</small><strong class="DB-Price"></strong> 
                <input type="radio" id="DB-4piston" checked="checked" name="brakes-type" class="custom_radio" data-elementName="Front & Rear Power Disc Brake System with four wheel Wilwood 4 Piston Calipers and Drilled & Slotted Rotors">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label title="Disc Brake 6 Piston" for="DB-6piston" class="label_container">Front & Rear Power Disc Brake System with Wilwood 6 Piston Front/4 Piston Rear Calipers and Drilled & Slotted Rotors <small>$</small><strong class="DB-Price"></strong>
                <input type="radio" id="DB-6piston" name="brakes-type" class="custom_radio" data-elementName="Front & Rear Power Disc Brake System with Wilwood 6 Piston Front/4 Piston Rear Calipers and Drilled & Slotted Rotors">
                <span class="checkmark"></span> </label>
            </div>
        </div>
        
 
                                                                    
                                                                    
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fa fa-dot-circle-o"></i>Interior</a> </h4>
                                                    </div>
                                                    <div id="collapse4" class="panel-collapse collapse">
                                                        <div class="panel-body"> 
                                                            <div class="option-view">
                                                                <img src="/wp-content/themes/Avada/images/options/Stock-Original-inter.jpg"> 
                                                            </div>
                                                            <h3 class="heading-options">Select Interior type</h3>
		<div class="dataRow"> 
            <div class="optBox">
                <label for="Stock-Original-inter" class="label_container">Stock Original Interior <strong>$11,475.00</strong>
                <input type="radio" id="Stock-Original-inter" checked="checked" name="interior-type" class="custom_radio" data-elementName="Stock Original Interior" data-pricetag="11475">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Performance-Style-c-inter" class="label_container">Performance Style Custom Interior <strong>$20,250.00</strong>
                <input type="radio" id="Performance-Style-c-inter" name="interior-type" class="custom_radio" data-elementName="Performance Style Custom Interior" data-pricetag="20250">
                <span class="checkmark"></span> </label>
            </div> 
            <div class="optBox">
                <label for="Custom-Luxury-inter" class="label_container">Custom Luxury Interior - <strong>Call for pricing</strong> on Custom Luxury Interiors
                <input type="radio" id="Custom-Luxury-inter" name="interior-type" class="custom_radio" data-elementName="Custom Luxury Interior" data-pricetag="Call for pricing">
                <span class="checkmark"></span> </label>
            </div>
        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                  
                                                 
                                                 
                                                
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse6B" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fa fa-television"></i>Extras</a> </h4>
                                                    </div>
                                                    <div id="collapse6B" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="option-view--">
                                                                <img src="/wp-content/themes/Avada/images/options/extra-options.jpg" style="max-width:100%"> 
                                                            </div>
                                                            <h3 class="heading-options">Select Extra Options</h3>
		<div class="dataRow"> 
            <div class="optBox">
                <label for="Climate-Control-ex" class="label_container">Climate Control
                <input type="checkbox" id="Climate-Control-ex" name="extra-options" class="custom_checkbox" data-optionName="Climate Control">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Windows-ex" class="label_container">Power Windows
                <input type="checkbox" id="Power-Windows-ex" name="extra-options" class="custom_checkbox" data-optionName="Power Windows">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Locks-ex" class="label_container">Keyless Entry Power Locks
                <input type="checkbox" id="Power-Locks-ex" name="extra-options" class="custom_checkbox" data-optionName="Keyless Entry Power Locks">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Locks-ss-ex" class="label_container">Keyless Entry Power Locks with Security System
                <input type="checkbox" id="Power-Locks-ss-ex" name="extra-options" class="custom_checkbox" data-optionName="Keyless Entry Power Locks with Security System">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Auto-Trunk-ex" class="label_container">Automatic Trunk Opening System
                <input type="checkbox" id="Auto-Trunk-ex" name="extra-options" class="custom_checkbox" data-optionName="Automatic Trunk Opening System">
                <span class="checkmark"></span> </label>
            </div> 
            <div class="optBox">
                <label for="Digital-Dash-ex" class="label_container">Digital Dash
                <input type="checkbox" id="Digital-Dash-ex" name="extra-options" class="custom_checkbox" data-optionName="Digital Dash">
                <span class="checkmark"></span> </label>
            </div>  
            <div class="optBox">
                <label for="Paddle-Shift-ex" class="label_container">Paddle Shift
                <input type="checkbox" id="Paddle-Shift-ex" name="extra-options" class="custom_checkbox" data-optionName="Paddle Shift">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Heated-Seats-ex" class="label_container">Heated Seats
                <input type="checkbox" id="Heated-Seats-ex" name="extra-options" class="custom_checkbox" data-optionName="Heated Seats">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Seats-ex" class="label_container">Power Seats
                <input type="checkbox" id="Power-Seats-ex" name="extra-options" class="custom_checkbox" data-optionName="Power Seats">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Media-Nav-System-ex" class="label_container">Media & Navigation System
                <input type="checkbox" id="Media-Nav-System-ex" name="extra-options" class="custom_checkbox" data-optionName="Media & Navigation System">
                <span class="checkmark"></span> </label>
            </div> 
            <div class="optBox">
                <label for="Mod-Sound-System-ex" class="label_container">Modern Sound System
                <input type="checkbox" id="Mod-Sound-System-ex" name="extra-options" class="custom_checkbox" data-optionName="Modern Sound System">
                <span class="checkmark"></span> </label>
            </div> 
            
        </div>                              
                                                        </div>                                  
                                                    </div>
                                                </div>
                                                
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse7B" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fa fa-car"></i>Packages</a> </h4>
                                                    </div>
                                                    <div id="collapse7B" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <h3 class="contant-hed">Equipment Groups</h3>
                                                            <div class="power-train-data"> 
                                                                <img src="/wp-content/themes/Avada/images/1-1.jpg">
                                                                <ul>
                                                                    <li><i class="fa fa-angle-right"></i>Rear View Camera</li>
                                                                    <li><i class="fa fa-angle-right"></i>TrackApps™</li>
                                                                    <li><i class="fa fa-angle-right"></i>LED Headlamps with LED Signature Lighting</li>
                                                                    <li><i class="fa fa-angle-right"></i>Intelligent Access with Push Button Start</li>
                                                                    <li><i class="fa fa-angle-right"></i>Dual Bright Exhaust with Rolled Tips </li>
                                                                </ul>
                                                                <span>Included</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>