<?php
/**
 * Template Name: Car Builder Page
 * Customizer Page Template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?> 
 
<link href="//use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet" type="text/css"> 
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="//fonts.googleapis.com/css?family=Karla:400,700" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="<?php echo site_url(); ?>/wp-content/themes/Avada/assets/css/style-customizer.css">
<link type="text/css" rel="stylesheet" href="<?php echo site_url(); ?>/wp-content/themes/Avada/assets/css/customize.css">
<link type="text/css" rel="stylesheet" href="<?php echo site_url(); ?>/wp-content/themes/Avada/assets/css/space.css">
<link type="text/css" rel="stylesheet" href="<?php echo site_url(); ?>/wp-content/themes/Avada/assets/css/fontawesome.min.css">

<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.js" integrity="sha256-EzjZPP7sOCQ+ugF/U8x2VH3PEvg3NFjis/dpwogN3T0=" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/Avada/assets/js/custom.js"></script> 

<style>
.page-template-car-builder-page .fusion-footer-widget-area { display: none; }
</style>

<div class="outerBody">
<!--<div class="loading" style="display:none"><img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/loader.gif" alt="Loading"> </div>-->
<div class="container-fluid">
  <div class="row">
    <div class="wizard"> 
      <ul class="nav nav-wizard">
        <li class="active"> <a href="#step1" data-toggle="tab">Choose Style</a> </li>
        <li class=""> <a id="customizerTab" href="javascript:void(0);">Build your dream</a> </li>
        <!--<li class="">
            <a href="#step2" data-toggle="tab">Shopping Cart<i class="fas fa-shopping-cart cart-size"></i></a>
        </li>-->
      </ul>
      <form>
        <div class="tab-content">
          <div class="tab-pane active" id="step1">
            <h2 class="category-hadding">Please choose one style below:</h2>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 category-tab-container">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 category-tab-menu">
                  <div class="list-group"> <a href="#" class="list-group-item active">
                    <h4 class="Muscle">High Performance Muscle Car Restomods</h4>
                    <i class="fas fa-angle-right "></i></a> <a href="#" class="list-group-item hide-panel">
                    <h4 class="Vintagel">Vintage style icons</h4>
                    <i class="fas fa-angle-right"></i></a> <a href="#" class="list-group-item hide-panel">
                    <h4 class="Classic">Classic Offroad </h4>
                    <i class="fas fa-angle-right"></i></a> <a href="#" class="list-group-item hide-panel">
                    <h4 class="Retro">Retro Cool</h4>
                    <i class="fas fa-angle-right"></i></a> <a href="#" class="list-group-item hide-panel">
                    <h4 class="Cruisers">Cruisers</h4>
                    <i class="fas fa-angle-right"></i></a>
                    <div class="stylenote">
                      <p>We can build any vehicle. Call for details.</p>
                    </div>
                  </div>
                </div>
                <div class="customWrar"></div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 category-tab"> 
                  <!-- section -->
                  <button class="accordion pad-responsive1">Muscle Car</button>
                  <div class="category-tab-content active panel-show">
                    <div class="row">
                      <!-- New_Car-->
                      <div class="col-xs-12 col-sm-12">
                        <div class="carBigbox" id="c1150" data-folder="c1_Corvette" data-roof="no" data-gloss="no" data-tire="yes" data-stripe="no" data-black-tire="yes" data-spoiler="no" data-color-type="no" data-interior="yes" data-moreRim="no" data-side-pipes="no" data-hood-scoop="no" data-sideColor="yes" data-Headlights="yes">
                          <h1 class="popup-head">C1 Corvette</h1>
                          <div class="content">
                            <div class="model_det">
                              <div class="det_para">
                                <h3 class="price-heading">
                                  <!--Starting  At<br><span class="priceCar">$ 134,485.00</span>-->
                                  <div class="customize_btn"> <a href="javascript:void(0);" class="btn-1 customizeNow" data-lease="200" data-basePrice="134485" data-name="Corvette" data-Brake4Price="7379.10" data-Brake6Price="7429.05"> Build Your Own<i class="fas fa-angle-right customize-arrow"></i></a> </div>
                                </h3>
                                <h4 class="key-heading">Restomod Details:</h4>
                                <p class="text-dark">For good reason, the C1 Corvette is the most popular restomod.  It’s the perfect combination of form and function.  And with GM’s LS line of crate engines, the only question becomes “How much horsepower?"</p>
                              </div>
                              <div> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/c1_Corvette/black.jpg" alt="" class="img-responsive"> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- New_Car-->
                      <div class="col-xs-12 col-sm-12">
                        <div class="carBigbox" id="car11" data-folder="56_57_c1_Corvette" data-roof="no" data-gloss="no" data-tire="yes" data-stripe="no" data-black-tire="yes" data-spoiler="no" data-color-type="no" data-interior="yes" data-moreRim="no" data-side-pipes="no" data-hood-scoop="no" data-sideColor="yes" data-Headlights="no">
                          <h1 class="popup-head">56-57 C1 Corvette</h1>
                          <div class="content">
                            <div class="model_det">
                              <div class="det_para">
                                <h3 class="price-heading">
                                  <!--Starting  At<br><span class="priceCar">$ 134,485.00</span>-->
                                  <div class="customize_btn"> <a href="javascript:void(0);" class="btn-1 customizeNow" data-lease="200" data-basePrice="134485" data-name="56-57 Corvette" data-Brake4Price="7379.10" data-Brake6Price="7429.05"> Build Your Own<i class="fas fa-angle-right customize-arrow"></i></a> </div>
                                </h3>
                                <h4 class="key-heading">Restomod Details:</h4>
                                <p class="text-dark">America’s sports car.  The subject of countless boyhood dreams.  Let us make your dream a reality.</p>
                              </div>
                              <div> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/56_57_c1_Corvette/black.jpg" alt="" class="img-responsive"> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--/New_Car-->
                      <div class="col-xs-12 col-sm-12">
                        <div class="carBigbox" id="car8" data-folder="retro-cool" data-roof="no" data-gloss="yes" data-tire="yes" data-stripe="yes" data-black-tire="yes" data-spoiler="yes" data-color-type="yes" data-interior="no" data-moreRim="no" data-side-pipes="no" data-hood-scoop="yes" data-Headlights="no">
                          <h1 class="popup-head">1967 Ford Mustang</h1>
                          <div class="content">
                            <div class="model_det">
                              <div class="det_para">
                                <h3 class="price-heading"> 
                                  <!--Starting  At<br><span class="priceCar">$ 22,000</span>-->
                                  <div class="customize_btn"> <a href="javascript:void(0);" class="btn-1 customizeNow" data-lease="300" data-basePrice="22000" data-name="Ford Mustang" data-Brake4Price="100" data-Brake6Price="120"> Build Your Own<i class="fas fa-angle-right customize-arrow"></i></a> </div>
                                </h3>
                                <h4 class="key-heading">Restomod Details:</h4>
                                <p class="text-dark">One of the most beloved muscle cars, let us build you a fire breathing beast of a pony car unlike any other on the road.</p>
                              </div>
                              <div> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/retro-cool/black.jpg" alt="" class="img-responsive model_img" id="model_img">  </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12">
                      <!--<div class="innerWrap"><span style="display:none;opacity:0;visibility:hidden">&nbsp;</span><p><input type="hidden" value="hideInput" id="readOnly"></p></div>-->
                        <div class="carBigbox" id="car2" data-folder="cruiser2" data-roof="no" data-gloss="no" data-tire="yes" data-stripe="yes" data-black-tire="yes" data-spoiler="yes" data-color-type="no" data-interior="no" data-moreRim="no" data-side-pipes="yes" data-hood-scoop="no" data-Headlights="no">
                          <h1 class="popup-head">C2 Corvette</h1>
                          <div class="content">
                            <div class="model_det">
                              <div class="det_para">
                                <h3 class="price-heading"> 
                                  <!--Starting  At<br><span class="priceCar">$ 134,485.00</span>-->
                                  <div class="customize_btn"> <a href="javascript:void(0);" class="btn-1 customizeNow" data-lease="200" data-basePrice="134485" data-name="Corvette" data-Brake4Price="7379.10" data-Brake6Price="7429.05"> Build Your Own<i class="fas fa-angle-right customize-arrow"></i></a> </div>
                                </h3>
                                <h4 class="key-heading">Restomod Details:</h4>
                                <p class="text-dark">Imagine the Stingray of your dreams sitting in your driveway.  With a swipe on your phone it roars to life...</p>
                              </div>
                              <div> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/cruiser2/black.jpg" alt="" class="img-responsive"> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12">
                        <div class="carBigbox" id="car6" data-folder="muscle-car3" data-roof="no" data-gloss="no" data-tire="yes" data-stripe="yes" data-black-tire="yes" data-spoiler="yes" data-color-type="no" data-interior="no" data-moreRim="no" data-side-pipes="no" data-hood-scoop="yes" data-Headlights="no">
                          <h1 class="popup-head">1968 Chevrolet Camaro</h1>
                          <div class="content">
                            <div class="model_det">
                              <div class="det_para">
                                <h3 class="price-heading"> 
                                  <!--Starting  At<br><span class="priceCar">$ 19,000</span>-->
                                  <div class="customize_btn"> <a href="javascript:void(0);" class="btn-1 customizeNow" data-lease="120" data-basePrice="120776" data-name="Chevrolet Camaro" data-Brake4Price="6252" data-Brake6Price="6327"> Build Your Own<i class="fas fa-angle-right customize-arrow"></i></a> </div>
                                </h3>
                                <h4 class="key-heading">Restomod Details:</h4>
                                <p class="text-dark">Nothing compares to the sharp lines of the first-generation Camaro... But compromising on modern amenities? Let us take that out of the equation.</p>
                              </div>
                              <div> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/muscle-car3/black.jpg" alt="" class="img-responsive model_img"> </div>
                            </div>
                          </div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <!-- section -->
                  <button class="accordion pad-responsive2">Vintage style icons</button>
                  <div class="category-tab-content panel-show">
                    <div class="row">

                      <!-- 190sl-Mercedes-->
                      <div class="col-xs-12 col-sm-12"> 
                        <div class="carBigbox" id="190sl-Mercedes" data-folder="190sl-Mercedes" data-roof="no" data-gloss="no" data-tire="yes" data-stripe="no" data-black-tire="yes" data-spoiler="no" data-color-type="no" data-interior="yes" data-moreRim="no" data-side-pipes="no" data-hood-scoop="no" data-Headlights="no" data-Interior-Color="yes" data-insetColor="yes" data-sideColor="yes">
                          <h1 class="popup-head">190sl Mercedes</h1>
                          <div class="content">
                            <div class="model_det">
                              <div class="det_para">
                                <h3 class="price-heading">  
                                  <div class="customize_btn"> <a href="javascript:void(0);" class="btn-1 customizeNow" data-lease="200" data-basePrice="134485" data-name="190sl Mercedes" data-Brake4Price="7379.10" data-Brake6Price="7429.05"> Build Your Own<i class="fas fa-angle-right customize-arrow"></i></a> </div>
                                </h3>
                                <h4 class="key-heading">Key Features:</h4>
                                <ul class="align">
                                  <li class="li_style">High performance custom frame</li>
                                  <li class="li_style">High performance GM LS or LT Drivetrain</li>
                                  <li class="li_style">Wilwood 4 or 6 piston brakes</li>
                                  <li class="li_style">Completely Customizable</li>
                                </ul>
                              </div>
                              <div> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/190sl-Mercedes/black.jpg" alt="" class="img-responsive"> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--/190sl-Mercedes-->

                      <div class="col-xs-12 col-sm-12">
                        <div class="carBigbox MercedesCar" id="car9" data-folder="retro2-cool" data-roof="no" data-gloss="no" data-tire="no" data-stripe="no" data-spoiler="no" data-color-type="no" data-interior="no" data-rim="no" data-moreRim="yes" data-side-pipes="no" data-hood-scoop="no" data-Headlights="no">
                          <h1 class="popup-head">Mercedes 300sl</h1>
                          <div class="content">
                            <div class="model_det">
                              <div class="det_para">
                                <h3 class="price-heading"> 
                                  <!--Starting  At<br><span class="priceCar">$ 28,000</span>-->
                                  <div class="customize_btn"> <a href="javascript:void(0);" class="btn-1 customizeNow" data-lease="320" data-basePrice="28000" data-name="Mercedes" data-Brake4Price="" data-Brake6Price=""> Build Your Own<i class="fas fa-angle-right customize-arrow"></i></a> </div>
                                </h3>
                                <h4 class="key-heading">Restomod Details:</h4>
                                <p class="text-dark">One of the most beautiful and iconic vehicles ever produced, the Mercedes Benz 300sl Gullwing.  Need we say anything more?</p>
                              </div>
                              <div> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/retro2-cool/black.jpg" alt="" class="img-responsive model_img"> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12">
                        <div class="carBigbox" id="car1" data-folder="cruiser" data-roof="no" data-gloss="no" data-tire="no" data-stripe="no" data-spoiler="no" data-color-type="no" data-interior="no" data-moreRim="no" data-side-pipes="no" data-hood-scoop="no" data-Headlights="no">
                          <h1 class="popup-head">356 Porsche</h1>
                          <div class="content">
                            <div class="model_det">
                              <div class="det_para">
                                <h3 class="price-heading"> 
                                  <!--Starting  At<br><span class="priceCar">$ 15,000</span>-->
                                  <div class="customize_btn"> <a href="javascript:void(0);" class="btn-1 customizeNow" data-lease="300" data-basePrice="15000" data-name="Porsche" data-Brake4Price="300" data-Brake6Price="320"> Build Your Own<i class="fas fa-angle-right customize-arrow"></i></a> </div>
                                </h3>
                                <h4 class="key-heading">Restomod Details:</h4>
                                <p class="text-dark">Driven by iconic owners such as Steve McQueen and Janis Joplin the 356 speedster needs no introduction. Join the club of speedster owners and make yours one of a kind.</p>
                              </div>
                              <div> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/cruiser/black.jpg" alt="" class="img-responsive model_img"> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12">
                        <div class="carBigbox" id="car3" data-folder="cruiser3" data-roof="no" data-gloss="no" data-tire="no" data-stripe="no" data-spoiler="no" data-color-type="no" data-interior="yes" data-moreRim="no" data-side-pipes="no" data-hood-scoop="no" data-Headlights="no">
                          <h1 class="popup-head">XK120 Jaguar</h1>
                          <div class="content">
                            <div class="model_det">
                              <div class="det_para">
                                <h3 class="price-heading"> 
                                  <!--Starting  At<br><span class="priceCar">$ 18,100</span>-->
                                  <div class="customize_btn"> <a href="javascript:void(0);" class="btn-1 customizeNow" data-lease="100" data-basePrice="18100" data-name="Jaguar" data-Brake4Price="400" data-Brake6Price="420"> Build Your Own<i class="fas fa-angle-right customize-arrow"></i></a> </div>
                                </h3>
                                <h4 class="key-heading">Restomod Details:</h4>
                                <p class="text-dark">The ultimate in British sophistication the XK120 Jaguar will make your presence known everywhere you go.</p>
                              </div>
                              <div> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/cruiser3/black.jpg" alt="" class="img-responsive model_img"> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- search -->
                  <button class="accordion pad-responsive3">Classic Offroad</button>
                  <div class="category-tab-content panel-show">
                    <div class="row">
                      <div class="col-xs-12 col-sm-12">
                        <div class="carBigbox" id="car7" data-folder="jeep" data-roof="yes" data-gloss="no" data-tire="no" data-stripe="no" data-spoiler="no" data-color-type="no" data-interior="no" data-moreRim="no" data-side-pipes="no" data-hood-scoop="no" data-Headlights="no">
                          <h1 class="popup-head">First Generation Bronco</h1>
                          <div class="content">
                            <div class="model_det">
                              <div class="det_para">
                                <h3 class="price-heading"> 
                                  <!--Starting  At<br><span class="priceCar">$ 10,000</span>-->
                                  <div class="customize_btn"> <a href="javascript:void(0);" class="btn-1 customizeNow" data-lease="60" data-basePrice="10000" data-name="First Generation Bronco" data-Brake4Price="500" data-Brake6Price="520"> Build Your Own<i class="fas fa-angle-right customize-arrow"></i></a> </div>
                                </h3>
                                <h4 class="key-heading">Restomod Details:</h4>
                                <p class="text-dark">Nothing begs for adventure more than a dirt road and the top down on a first gen Bronco.</p>
                              </div>
                              <div> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/jeep/black.jpg" alt="" class="img-responsive model_img"> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- search -->
                  <button class="accordion pad-responsive4">Retro Cool</button>
                  <div class="category-tab-content panel-show">
                    <div class="row">
                      <div class="col-xs-12 col-sm-12">
                        <div class="carBigbox" id="car4" data-folder="muscle-car" data-roof="yes" data-gloss="no" data-tire="yes" data-stripe="no" data-black-tire="no" data-spoiler="no" data-color-type="no" data-interior="no" data-moreRim="no" data-side-pipes="no" data-hood-scoop="no" data-Headlights="no">
                          <h1 class="popup-head">1932 Ford Roadster</h1>
                          <div class="content">
                            <div class="model_det">
                              <div class="det_para">
                                <h3 class="price-heading"> 
                                  <!--Starting  At<br><span class="priceCar">$ 12,000</span>-->
                                  <div class="customize_btn"> <a href="javascript:void(0);" class="btn-1 customizeNow" data-lease="500" data-basePrice="12000" data-name="1932 Ford Roadster" data-Brake4Price="600" data-Brake6Price="620"> Build Your Own<i class="fas fa-angle-right customize-arrow"></i></a> </div>
                                </h3>
                                <h4 class="key-heading">Restomod Details:</h4>
                                <p class="text-dark">The three-window 1932 Ford coupe is the definition of a Hot Rod but could it be more than that?  Let us make it your definition of freedom.</p>
                              </div>
                              <div> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/muscle-car/black.jpg" alt="" class="img-responsive model_img"> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- search-bar for the accordain panel for responsive cars -->
                  <button class="accordion pad-responsive5">Cruisers</button>
                  <div class="category-tab-content panel-show">
                    <div class="row">
                      <div class="col-xs-12 col-sm-12">
                        <div class="carBigbox" id="car5" data-folder="muscle-car2" data-roof="yes" data-gloss="no" data-tire="no" data-stripe="no" data-spoiler="no" data-color-type="no" data-interior="no" data-moreRim="no" data-side-pipes="no" data-hood-scoop="no" data-Headlights="no">
                          <h1 class="popup-head">1967 Lincoln Continental</h1>
                          <div class="content">
                            <div class="model_det">
                              <div class="det_para">
                                <h3 class="price-heading"> 
                                  <!--Starting  At<br><span class="priceCar">$ 25,000</span>-->
                                  <div class="customize_btn"> <a href="javascript:void(0);" class="btn-1 customizeNow" data-lease="410" data-basePrice="25000" data-name="Continental" data-Brake4Price="700" data-Brake6Price="720"> Build Your Own<i class="fas fa-angle-right customize-arrow"></i></a> </div>
                                </h3>
                                <h4 class="key-heading">Restomod Details:</h4>
                                <p class="text-dark">With its suicide doors, this generation of Lincoln Continental is the ultimate in style.  Everywhere you go becomes a red carpet premier.</p>
                              </div>
                              <div> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/muscle-car2/black.jpg" alt="" class="img-responsive model_img"> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- search --> 
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="customizeArea">
            <div class="row">
              <div class="col-sm-8 col-md-8 left-sidebar">
                <div class="top-heading">
                  <div class="price"> <a href="#body-color" class="btn btn-info active" data-toggle="tab">$<span class="carPrice">20,000</span></br>
                    Estimated Net Price</a> </div>
                  <div class="lecense btn">$
                    <input type="checkbox" id="check">
                    <label for="check" class="leasePrice">304</label>
                    <br>
                    Per Month for 39 Month Lease </div>
                  <div class="call"> <a href="tel:5137721896" class="btn btn-info active">Have vehicle questions?</br>
                    Call (513) 772-4105</a> </div>
                </div>
                <div class="sticky-wrapper is-sticky">
                <div class="loading" style="display: none;"><img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/loader.gif" alt="Loadig..."></div>
                  <div class="customizer-container"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/jeep/orignal.jpg" class="img-change"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-rim"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-roof"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-tire"><img  src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-interiorColor"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-stripe"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-spoiler"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-interior"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-moreRim"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-sidePipes"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-hoodScoope"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/c1_Corvette/transparent.png" class="img-HL-chrome" /> </div>
                </div>
              </div>
              <div class="col-sm-4 col-md-4 right-sidebar">
                <div class="top-heading">
                  <h1>Options</h1>
                  <div id="toolbox">
                    <div class="sideScroller">
                      
                        <div id="ajxData"></div>
                      <!--</div>-->
                      <div class="box"> <a class="button openPop" href="javascript:void(0);" id="orderSummary" data-modal="#popup1">Summary</a>
                      <!--<a class="button openPop" href="javascript:void(0);" id="driveIn" data-modal="#popup2">Drive in</a>--> </div>
                    </div>
                    <div id="popup1" class="overlay">
                      <div class="customPopup">
                        <div class="popHead">
                          <h1 class="popup-hed">Your Car</h1>
                          <a class="close" href="javascript:void(0);">×</a> </div>
                        <div class="content popContent">
                          <div class="row">
                            <div class="col-md-6"> 
								<!--<h3>Payment</h3>
                                <hr>
                                <ul>
                                    <li class="margin-top-10"> 
                                        <span class="summary-price"><small>$</small><strong class="bPrice addCommas" id="b_price">0</strong></span>
                                        <span class="summary-item"><strong>Base Price</strong></span>
                                    </li>
                                    <li class="margin-top-10 cfp"> 
                                        <span class="summary-price"><small>$</small><strong class="optionAdded addCommas" id="optAdd">0</strong></span>
                                        <span class="summary-item"><strong>Total of Options</strong></span> 
                                    </li>
                                                    
                                    <li class="margin-top-10" id="leaseBind"> 
                                        <span class="summary-price"><small>$</small><strong class="leasePrice addCommas" id="l_price">0</strong></span> 
                                        <span class="summary-item"><strong>Lease</strong></span> 
                                    </li>                        
                                </ul>
                                <h3>Total Price Altogether</h3>
                                <hr>
                                <ul>
                                    <li class="margin-top-10 cfp"> 
                                        <span class="summary-price"><small>$</small><strong class="totalPrice addCommas" id="t_price"> 0</strong></span> 
                                        <span class="summary-item"><strong>Estimated Net Price</strong>
                                        <p class="addition_note">Plus<br>
                                        Metalwork, bodywork & Paint Time & Material<br>
                                        Extra Features Time & Material</p>
                                        </span>
                                    </li> 
                                </ul>-->
                              
                              <div class="drive-in" id="drivein" style="margin:20px 0"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/jeep/orignal.jpg" class="driveImg"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-rim"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-roof"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-tire"><img  src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-interiorColor"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-stripe"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-spoiler"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-interior"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-moreRim"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-sidePipes"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-hoodScoope"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/c1_Corvette/transparent.png" class="img-HL-chrome" /> </div>
                              <div class="imp_note">
                                <p><strong>Note: </strong><!--This price is an estimate. -->This vehicle is completely customizable. Please submit this build and schedule a free consultation to discuss your project.</p>
                                
                                              
								</div>
                                
                                 <div id="form" class="mailForm">
                                    <h3>Contact Form</h3>  
                                    <div class="fieldsWrap"> 
                                        <div class="inputsWrap">
                                            <input class="txtfld" type="text" id="name" placeholder="Name"/> 
                                            <input class="emailfld" type="text" id="email" placeholder="Email"/> 
                                            <input class="phfld" type="text" id="contact" placeholder="Mobile Number"/> 
                                        </div>
                                    </div>
                                    <textarea id="message" placeholder="Custom Extras"></textarea>
                                    <input type="button" id="submit" value="Submit Build"/>
                                    <div class="validatMsg msgF3" style="display:none"></div>
                                    <p id="returnmessage"></p>  
                                </div>  



                              
                            </div>
                            <div class="col-md-6">
                              <h3>Model</h3>
                              <hr>
                              <ul>
                                <li class="margin-top-10"> <span class="summary-price"><small>$</small><strong class="carMainPrice addCommas">0</strong></span> <span class="summary-item"><strong class="carTitle">Ford 1960 Mustang</strong></span> </li>
                              </ul>
                              <h3>Paint and Exterior</h3>
                              <hr>
                              <ul class="cfp">
                                <li class="margin-top-10" id="paintTag"> <span class="summary-price"><small>$</small><strong class="paintPrice"> 0</strong></span> <span class="summary-item">Paint: <strong class="paintName">No Paint is selected.</strong></span> </li>
                                <li class="margin-top-10"> <span class="summary-price"><small>$</small><strong> 0</strong></span> <span class="summary-item"><strong class="wheelsName"></strong></span> </li>
                                <li class="margin-top-10"> <span class="summary-price"><small>$</small><strong> 0</strong></span> <span class="summary-item">Wheel Color: <strong class="wheelsColor"></strong></span> </li>
                                <li class="margin-top-10"> <span class="summary-price"><small>$</small><strong> 0</strong></span> <span class="summary-item">Stripe Color: <strong class="stripeColor"></strong></span> </li>
                                <li class="margin-top-10"> <span class="summary-price"><small>$</small><strong> 0</strong></span> <span class="summary-item"><strong class="spoilerCheck"></strong></span> </li>
                                <li class="margin-top-10"> <span class="summary-price"><small>$</small><strong> 0</strong></span> <span class="summary-item"><strong class="HoodScoopCheck"></strong></span> </li>
                                <!--li class="addition_note">NOTE: All metalwork, bodywork and paint will be charged for time & material in addition to these prices</li-->
                              </ul>
                              <h3>PowerTrain</h3>
                              <hr>
                              <ul class="cfp">
                                <li class="margin-top-10"> <span class="summary-price"><small>$</small><strong class="engPrice"> 0</strong></span> <span class="summary-item"><strong class="engName">NO Engine type is selected.</strong></span> </li>
                              </ul>
                              <h3>Brakes</h3>
                              <hr>
                              <ul class="cfp">
                                <li class="margin-top-10"> <span class="summary-price"><small>$</small><strong class="brakePrice"> 0</strong></span> <span class="summary-item"><strong  class="brakeName">No Brake type is selected.</strong></span> </li>
                              </ul>
                              <h3>Interior</h3>
                              <hr>
                              <ul class="cfp">
                                <li class="margin-top-10"> <span class="summary-price"><small>$</small><strong class="interPrice"> 0</strong></span> <span class="summary-item"><strong class="interName">No Interior type is selected.</strong></span> </li>
                              </ul> 
                              <h3>Extras</h3>
                              <hr>
                              <ul class="ex-SelctedOptions">
                                <!--li class="addition_note">NOTE: Extras will be charged for time & material in addition to these prices</li-->
                              </ul>
                              <h3>Packages</h3>
                              <hr>
                              <ul class="cfp">
                                <li class="margin-top-10"><span class="summary-price"><small>$</small><strong> 0</strong></span> <span class="summary-item"><strong>200A</strong></span> </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="popup2" class="overlay">
                    <div class="customPopup">
                      <div class="popHead">
                        <h1 class="popup-hed">Find your dream car</h1>
                        <a class="close" href="javascript:void(0);">×</a> </div>
                      <div class="content popContent">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="drive-in"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/jeep/orignal.jpg" class="driveImg"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-rim"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-roof"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-tire"> <img  src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-interiorColor"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-stripe"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-spoiler"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-interior"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-moreRim"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-sidePipes"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/transparent.png" class="img-hoodScoope"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/c1_Corvette/transparent.png" class="img-HL-chrome" /> </div>
                            <h3>Bronco Ford</h3>
                            <p class="no_pad">Dealership Selection</p>
                            <div class="dealer">
                              <select id="dealer-ship" name="dealers">
                                <option value="australia">Bronco Ford 3.7 miles away</option>
                                <option value="canada">Bronco Ford 4.5 miles away</option>
                                <option value="usa">Bronco Ford 3.2 miles away</option>
                                <option value="uae">Bronco Ford 5.6 miles away</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="contact-form">
                              <p>First name</p>
                              <input id="fName" type="text" name="firstname" value="">
                              <br>
                              <p>Last name</p>
                              <input id="lName" type="text" name="lastname" value="">
                              <br>
                              <p>Email</p>
                              <input id="emailadd" type="text" name="emailadd" value="">
                              <br>
                              <p>Phone</p>
                              <input id="phonenmbr" type="text" name="phoneno" value="">
                              <br>
                              <p>Zipe Code</p>
                              <input id="zipcord" type="text" name="zipcord" value="">
                              <br>
                              <a id="getPriceBtn" href="javascript:void(0);">Get an Internet Price</a>
                              <div class="validatMsg msgF2" style="display:none;margin:0"><p>Please fill all the fields with valid information.</p></div>
                              <div class="successPopBtn openPop" data-modal="#successPopup" style="display:none"></div>
                              <div class="checkbox-box">
                                <label for="fco" class="label_container">Include Ford Credit Offers
                                  <input type="checkbox" id="fco" class="custom_checkbox">
                                  <span class="checkmark"></span> </label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--<div class="col-md-12">
            <div class="tabbable">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#one" data-toggle="tab">Description</a></li>
                    <li><a href="#two" data-toggle="tab">Description 2</a></li>
                    <li><a href="#twee" data-toggle="tab">Description3</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="one">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                    <div class="tab-pane" id="two">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                    <div class="tab-pane" id="twee">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>                        
                    </div>
                </div>
            </div> 
			</div>    --> 
          </div>
          <div class="tab-pane" id="step2" style="display: none;">
            <div class="shopping-cart">
              <div class="title"> Shopping Cart </div>
              <div class="item list-1">
                <div class="buttons"> <span class="delete-btn" id="d-btn1"> <svg width="14px" height="14px" viewBox="0 0 14 14" version="1.1" xmlns="//www.w3.org/2000/svg" xmlns:xlink="//www.w3.org/1999/xlink">
                  <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Ecommerce" transform="translate(-180.000000, -5886.000000)" fill="#BDC7CF">
                      <g id="Cart-#1-Copy" transform="translate(150.000000, 5772.000000)">
                        <g id="#1" transform="translate(30.000000, 81.000000)">
                          <path d="M12.3880257,33.3338025 L7.45682327,38.2650049 C7.25459731,38.4667123 6.92792459,38.4667123 6.72621715,38.2650049 L1.61249282,33.1512806 C1.41078538,32.9495731 1.08359414,32.9495731 0.881886702,33.1512806 L0.151280579,33.8818867 C-0.0504268598,34.0835941 -0.0504268598,34.4102669 0.151280579,34.6124928 L5.26500491,39.7262172 C5.46671235,39.9279246 5.46671235,40.2545973 5.26500491,40.4568233 L0.333802478,45.3875072 C0.132095039,45.5892146 0.132095039,45.9164059 0.333802478,46.1181133 L1.0644086,46.8487194 C1.26611604,47.0504269 1.59330728,47.0504269 1.79501472,46.8487194 L6.72621715,41.917517 C6.92792459,41.7158096 7.25459731,41.7158096 7.45682327,41.917517 L12.2049853,46.6661975 C12.4066927,46.867905 12.733884,46.867905 12.9355914,46.6661975 L13.6661975,45.9355914 C13.867905,45.733884 13.867905,45.4066927 13.6661975,45.2049853 L8.91751699,40.4568233 C8.71580955,40.2545973 8.71580955,39.9279246 8.91751699,39.7262172 L13.8487194,34.7950147 C14.0504269,34.5933073 14.0504269,34.266116 13.8487194,34.0644086 L13.1181133,33.3338025 C12.9164059,33.132095 12.5897331,33.132095 12.3880257,33.3338025" id="Icon"></path>
                        </g>
                      </g>
                    </g>
                  </g>
                  </svg> </span> <span class="like-btn"></span> </div>
                <div class="image"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/black.jpg" alt=""  /> </div>
                <div class="description"> <span>1970 Corvette</span> <span>Locomotive</span> <span>Black</span> </div>
                <div class="quantity">
                  <button class="plus-btn" type="button" name="button"> <svg width="10px" height="10px" viewBox="0 0 10 10" version="1.1" xmlns="//www.w3.org/2000/svg" xmlns:xlink="//www.w3.org/1999/xlink">
                  <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Ecommerce" transform="translate(-649.000000, -5888.000000)" fill="#86939E">
                      <g id="Cart-#1-Copy" transform="translate(150.000000, 5772.000000)">
                        <g id="#1" transform="translate(30.000000, 81.000000)">
                          <g id="QTY" transform="translate(459.000000, 25.000000)">
                            <path d="M14,14 L10,14 L10,16 L14,16 L14,20 L16,20 L16,16 L20,16 L20,14 L16,14 L16,10 L14,10 L14,14 Z" id="Icon"></path>
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  </svg> </button>
                  <input type="text" name="name" value="1">
                  <button class="minus-btn" type="button" name="button"> <svg width="10px" height="2px" viewBox="0 0 10 2" version="1.1" xmlns="//www.w3.org/2000/svg" xmlns:xlink="//www.w3.org/1999/xlink">
                  <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Ecommerce" transform="translate(-722.000000, -5892.000000)" fill="#86939E">
                      <g id="Cart-#1-Copy" transform="translate(150.000000, 5772.000000)">
                        <g id="#1" transform="translate(30.000000, 81.000000)">
                          <g id="QTY" transform="translate(459.000000, 25.000000)">
                            <rect id="Icon" x="83" y="14" width="10" height="2"></rect>
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  </svg> </button>
                </div>
                <div class="total-price">$22,000</div>
              </div>
              <div class="item list-2">
                <div class="buttons"> <span class="delete-btn" id="d-btn2"> <svg width="14px" height="14px" viewBox="0 0 14 14" version="1.1" xmlns="//www.w3.org/2000/svg" xmlns:xlink="//www.w3.org/1999/xlink">
                  <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Ecommerce" transform="translate(-180.000000, -5886.000000)" fill="#BDC7CF">
                      <g id="Cart-#1-Copy" transform="translate(150.000000, 5772.000000)">
                        <g id="#1" transform="translate(30.000000, 81.000000)">
                          <path d="M12.3880257,33.3338025 L7.45682327,38.2650049 C7.25459731,38.4667123 6.92792459,38.4667123 6.72621715,38.2650049 L1.61249282,33.1512806 C1.41078538,32.9495731 1.08359414,32.9495731 0.881886702,33.1512806 L0.151280579,33.8818867 C-0.0504268598,34.0835941 -0.0504268598,34.4102669 0.151280579,34.6124928 L5.26500491,39.7262172 C5.46671235,39.9279246 5.46671235,40.2545973 5.26500491,40.4568233 L0.333802478,45.3875072 C0.132095039,45.5892146 0.132095039,45.9164059 0.333802478,46.1181133 L1.0644086,46.8487194 C1.26611604,47.0504269 1.59330728,47.0504269 1.79501472,46.8487194 L6.72621715,41.917517 C6.92792459,41.7158096 7.25459731,41.7158096 7.45682327,41.917517 L12.2049853,46.6661975 C12.4066927,46.867905 12.733884,46.867905 12.9355914,46.6661975 L13.6661975,45.9355914 C13.867905,45.733884 13.867905,45.4066927 13.6661975,45.2049853 L8.91751699,40.4568233 C8.71580955,40.2545973 8.71580955,39.9279246 8.91751699,39.7262172 L13.8487194,34.7950147 C14.0504269,34.5933073 14.0504269,34.266116 13.8487194,34.0644086 L13.1181133,33.3338025 C12.9164059,33.132095 12.5897331,33.132095 12.3880257,33.3338025" id="Icon"></path>
                        </g>
                      </g>
                    </g>
                  </g>
                  </svg> </span> <span class="like-btn"></span> </div>
                <div class="image"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/cream.jpg" alt=""/> </div>
                <div class="description"> <span>C2 Corvette</span> <span>High Speed</span> <span>Cream</span> </div>
                <div class="quantity">
                  <button class="plus-btn" type="button" name="button"> <svg width="10px" height="10px" viewBox="0 0 10 10" version="1.1" xmlns="//www.w3.org/2000/svg" xmlns:xlink="//www.w3.org/1999/xlink">
                  <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Ecommerce" transform="translate(-649.000000, -5888.000000)" fill="#86939E">
                      <g id="Cart-#1-Copy" transform="translate(150.000000, 5772.000000)">
                        <g id="#1" transform="translate(30.000000, 81.000000)">
                          <g id="QTY" transform="translate(459.000000, 25.000000)">
                            <path d="M14,14 L10,14 L10,16 L14,16 L14,20 L16,20 L16,16 L20,16 L20,14 L16,14 L16,10 L14,10 L14,14 Z" id="Icon"></path>
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  </svg> </button>
                  <input type="text" name="name" value="1">
                  <button class="minus-btn" type="button" name="button"> <svg width="10px" height="2px" viewBox="0 0 10 2" version="1.1" xmlns="//www.w3.org/2000/svg" xmlns:xlink="//www.w3.org/1999/xlink">
                  <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Ecommerce" transform="translate(-722.000000, -5892.000000)" fill="#86939E">
                      <g id="Cart-#1-Copy" transform="translate(150.000000, 5772.000000)">
                        <g id="#1" transform="translate(30.000000, 81.000000)">
                          <g id="QTY" transform="translate(459.000000, 25.000000)">
                            <rect id="Icon" x="83" y="14" width="10" height="2"></rect>
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  </svg> </button>
                </div>
                <div class="total-price">$22,000</div>
              </div>
              <div class="item list-3">
                <div class="buttons"> <span class="delete-btn" id="d-btn3"> <svg width="14px" height="14px" viewBox="0 0 14 14" version="1.1" xmlns="//www.w3.org/2000/svg" xmlns:xlink="//www.w3.org/1999/xlink">
                  <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Ecommerce" transform="translate(-180.000000, -5886.000000)" fill="#BDC7CF">
                      <g id="Cart-#1-Copy" transform="translate(150.000000, 5772.000000)">
                        <g id="#1" transform="translate(30.000000, 81.000000)">
                          <path d="M12.3880257,33.3338025 L7.45682327,38.2650049 C7.25459731,38.4667123 6.92792459,38.4667123 6.72621715,38.2650049 L1.61249282,33.1512806 C1.41078538,32.9495731 1.08359414,32.9495731 0.881886702,33.1512806 L0.151280579,33.8818867 C-0.0504268598,34.0835941 -0.0504268598,34.4102669 0.151280579,34.6124928 L5.26500491,39.7262172 C5.46671235,39.9279246 5.46671235,40.2545973 5.26500491,40.4568233 L0.333802478,45.3875072 C0.132095039,45.5892146 0.132095039,45.9164059 0.333802478,46.1181133 L1.0644086,46.8487194 C1.26611604,47.0504269 1.59330728,47.0504269 1.79501472,46.8487194 L6.72621715,41.917517 C6.92792459,41.7158096 7.25459731,41.7158096 7.45682327,41.917517 L12.2049853,46.6661975 C12.4066927,46.867905 12.733884,46.867905 12.9355914,46.6661975 L13.6661975,45.9355914 C13.867905,45.733884 13.867905,45.4066927 13.6661975,45.2049853 L8.91751699,40.4568233 C8.71580955,40.2545973 8.71580955,39.9279246 8.91751699,39.7262172 L13.8487194,34.7950147 C14.0504269,34.5933073 14.0504269,34.266116 13.8487194,34.0644086 L13.1181133,33.3338025 C12.9164059,33.132095 12.5897331,33.132095 12.3880257,33.3338025" id="Icon"></path>
                        </g>
                      </g>
                    </g>
                  </g>
                  </svg> </span> <span class="like-btn"></span> </div>
                <div class="image"> <img src="<?php echo site_url(); ?>/wp-content/themes/Avada/images/dark-blue.jpg" alt="" /> </div>
                <div class="description"> <span>C2 Corvette</span> <span>High Speed</span> <span>Dark Blue</span> </div>
                <div class="quantity">
                  <button class="plus-btn" type="button" name="button"> <svg width="10px" height="10px" viewBox="0 0 10 10" version="1.1" xmlns="//www.w3.org/2000/svg" xmlns:xlink="//www.w3.org/1999/xlink">
                  <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Ecommerce" transform="translate(-649.000000, -5888.000000)" fill="#86939E">
                      <g id="Cart-#1-Copy" transform="translate(150.000000, 5772.000000)">
                        <g id="#1" transform="translate(30.000000, 81.000000)">
                          <g id="QTY" transform="translate(459.000000, 25.000000)">
                            <path d="M14,14 L10,14 L10,16 L14,16 L14,20 L16,20 L16,16 L20,16 L20,14 L16,14 L16,10 L14,10 L14,14 Z" id="Icon"></path>
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  </svg> </button>
                  <input type="text" name="name" value="1">
                  <button class="minus-btn" type="button" name="button"> <svg width="10px" height="2px" viewBox="0 0 10 2" version="1.1" xmlns="//www.w3.org/2000/svg" xmlns:xlink="//www.w3.org/1999/xlink">
                  <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Ecommerce" transform="translate(-722.000000, -5892.000000)" fill="#86939E">
                      <g id="Cart-#1-Copy" transform="translate(150.000000, 5772.000000)">
                        <g id="#1" transform="translate(30.000000, 81.000000)">
                          <g id="QTY" transform="translate(459.000000, 25.000000)">
                            <rect id="Icon" x="83" y="14" width="10" height="2"></rect>
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  </svg> </button>
                </div>
                <div class="total-price">$22,000</div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  
</div>
<div id="successPopup" class="overlay delevir_summary">
  <div class="customPopup">
    <div class="popHead thanksMsg">
      <p><i class="far fa-check-circle"></i> Thank you for submitting your build. We will contact you shortly to schedule a consultation.</p>
      <!--<a class="close" href="javascript:void(0);">×</a>--> </div>
  </div>
</div>
</div>
 
<?php
get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
