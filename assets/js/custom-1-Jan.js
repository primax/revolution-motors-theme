/*function setHeight(){
	"use strict";
	var windowHeight = jQuery(window).innerHeight();
	jQuery('#toolbox').css('height', windowHeight - 0);
}jQuery(document).ready(function(){
	"use strict";
	setHeight();
});
jQuery(window).resize(function(){
	"use strict";
	setHeight();
});*/
 
/* Car Images Folder Code Start

----------------------------*/


/* Ajax code START
------------------------*/

$ = jQuery; //Dollar sign vs jQuery imr
var siteURL = '//www.revo-motors.com/wp-content/themes/Avada/';   //Set Website Base URL Here imr


jQuery(document).on('click','.customizeNow',function(){ 
var folderName = jQuery(this).closest('.carBigbox').attr('data-folder');
$.ajax({
    url: siteURL+"/options/"+folderName+"/options.php",
    dataType: 'html', // type of file (text, json, xml, etc)
    success: function(data) { // callback for successful completion
      $("#ajxData").html(data); 
 
//--Moved Code from success END------------------
jQuery(document).on('click','.customizeNow',function(){
	var Brake4Price     = jQuery(this).attr('data-Brake4Price');
	var Brake6Price     = jQuery(this).attr('data-Brake6Price');
	jQuery('#DB-4piston').prev('strong').text(Brake4Price);
	jQuery('#DB-6piston').prev('strong').text(Brake6Price); 
});

jQuery(document).on('click','.optBox .custom_radio',function(){
	"use strict";
	var parentDiv = $(this).closest('.panel');
	var targetDiv = parentDiv.children('.panel-collapse').children('.panel-body').children('.option-view');
	var imgName = jQuery(this).attr('id');
	var engImgSrc = siteURL+'images/options/'+imgName+'.jpg';
	targetDiv.children('img').attr("src" , engImgSrc);
});

jQuery('input[name="Engine-and-Trans"]').each(function() {
	var elementName   = jQuery(this).attr('data-elementName');
	var elementPrice  = jQuery(this).attr('data-priceTag'); 
	if (jQuery(this).prop('checked')) {
		jQuery('.engName').text(elementName); 
		jQuery('.engPrice').text(elementPrice); 
	}   
});
jQuery('input[name="Engine-and-Trans"]').change(function() {
	var elementName   = jQuery(this).attr('data-elementName');
	var elementPrice  = jQuery(this).attr('data-priceTag'); 
	if (jQuery(this).prop('checked')) { 
		jQuery('.engName').text(elementName); 
		jQuery('.engPrice').text(elementPrice); 
	}   
});
 
jQuery('input[name="brakes-type"]').each(function() {
	var elementName   = jQuery(this).attr('data-elementName');  
	if (jQuery(this).prop('checked')) {
		jQuery('.brakeName').text(elementName);  
	}  
});
jQuery('input[name="brakes-type"]').change(function() {
	var elementName   = jQuery(this).attr('data-elementName'); 
	if (jQuery(this).prop('checked')) { 
		jQuery('.brakeName').text(elementName);   
	} 
});

jQuery('input[name="interior-type"]').each(function() {
	var elementName   = jQuery(this).attr('data-elementName');
	var elementPrice  = jQuery(this).attr('data-priceTag');  
	if (jQuery(this).prop('checked')) {
		jQuery('.interName').text(elementName);
		jQuery('.interPrice').text(elementPrice);
	}
});
jQuery('input[name="interior-type"]').change(function() {
	var elementName   = jQuery(this).attr('data-elementName');
	var elementPrice  = jQuery(this).attr('data-priceTag');  
	if (jQuery(this).prop('checked')) { 
		jQuery('.interName').text(elementName);
		jQuery('.interPrice').text(elementPrice);
	}
});
 
jQuery('input[name="extra-options"]').each(function(){
	if (jQuery(this).prop('checked')) {
		 var elementName   = jQuery(this).attr('data-optionName');
		 var elementId   = jQuery(this).attr('id');
		 jQuery('.ex-SelctedOptions').prepend('<li class="'+elementId+'-c">'+elementName+'</li>');  
	} 
	else {
		jQuery('.ex-SelctedOptions').html('<li class="no-options">No any option is selected.</li><li class="addition_note">NOTE: Extras will be charged for time & material in addition to these prices</li>');
	}
});
jQuery('input[name="extra-options"]').change(function(){ 
	var elementName   = jQuery(this).attr('data-optionName'); 
	var elementId   = jQuery(this).attr('id'); 
	if (jQuery(this).prop('checked')) { 
		jQuery('.ex-SelctedOptions').prepend('<li class="'+elementId+'-c">'+elementName+'</li>');  
		jQuery('.no-options').remove(); 
	} 
	else{
		jQuery('.ex-SelctedOptions .'+elementId+'-c').remove(); 
	} 
});

jQuery('input[name="color-4"]').each(function(){ 
if (jQuery(this).prop('checked')) {
	var wheelsName   = jQuery(this).attr('data-wheelsName');  
	jQuery('.wheelsName').text(wheelsName);
}
});
jQuery('input[name="color-2"]').each(function(){
if (jQuery(this).prop('checked')) { 
	var wheelsColor	= jQuery(this).attr('data-wheelsColor'); 	
	jQuery('.wheelsColor').text(wheelsColor);
}
});
jQuery('input[name="color-5"]').each(function(){
if (jQuery(this).prop('checked')) { 
	var stripeColor	= jQuery(this).attr('data-stripeColor'); 	
	jQuery('.stripeColor').text(stripeColor);
}
});
jQuery('input[name="color-7"]').each(function(){
if (jQuery(this).prop('checked')) { 
	var spoilerCheck = jQuery(this).attr('data-spoiler'); 
	jQuery('.spoilerCheck').text(spoilerCheck);
}
});
jQuery('input[name="color-12"]').each(function(){
if (jQuery(this).prop('checked')) { 
	var HoodScoopCheck = jQuery(this).attr('data-hood-scoop'); 
	jQuery('.HoodScoopCheck').text(HoodScoopCheck);
}
});
jQuery('input[name="HL-type"]').each(function(){ 
if (jQuery(this).prop('checked')) {
	var headlightsCheck = jQuery(this).attr('data-hlType');
	jQuery('.headlightsCheck').text(headlightsCheck);
}
});
jQuery('input[name="interior-color"]').each(function(){ 
if (jQuery(this).prop('checked')) {
	var interiorColor	= jQuery(this).attr('data-intercolor');
	jQuery('.interiorColor').text(interiorColor);
}
});

jQuery(document).on('click','.wizard>.nav-wizard>li>a[data-toggle="tab"]', function(){ 
	jQuery('.img-change').attr('src',siteURL+'images/transparent.png');
	jQuery('#tool_tire').show();
	jQuery('#img-tire').show(); 
});

/* Color-type Code Start
	--------------------------*/ 
	jQuery("#stockColor").on('click', function(){
	jQuery('#tool_paint').show();
	jQuery('#tool_paint_gloss').hide(); 
	jQuery('input#color01').trigger('click');
	jQuery('input#color00').trigger('click'); 
	});
	jQuery("#glossColor").on('click',function(){
	jQuery('#tool_paint_gloss').show();
	jQuery('#tool_paint').hide(); 
	jQuery('input#color31').trigger('click');
	jQuery('input#color01').trigger('click'); 
	jQuery('input#color31').attr('checked','checked');
	});  
	jQuery('input[name="color-8"]').each(function() {
	if (jQuery('#stockColor').is(':checked')) { 
	jQuery('#tool_paint').show();
	jQuery('#tool_paint_gloss').hide(); 
	jQuery('input#color01').trigger('click');
	jQuery('input#color01').attr('checked','checked');
	} 
	if (jQuery('#glossColor').is(':checked')) {
	jQuery('#tool_paint_gloss').show();
	jQuery('#tool_paint').hide(); 
	jQuery('input#color31').trigger('click');
	jQuery('input#color31').attr('checked','checked');
	}
	});
		
/* Color-type Code Ends
--------------------------*/


 


jQuery(document).ready(function(){
	
	jQuery('input#color00').trigger('click');
	
	jQuery('input[name="color"]').on('click',function(){
		
		showLoader(); 
		var imgFolder	= jQuery('#body-color').attr('data-folder'),
			imgColor	= jQuery(this).attr('data-color'),
			currentImg	= siteURL+'images/'+imgFolder+'/'+imgColor; 	
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-change").attr('src',currentImg);
			  hideLoader();
			};
			image.src = currentImg;
		jQuery('.driveImg').attr('src',currentImg);
		var paintTool   = jQuery(this).attr('data-colorName');
		jQuery('.paintName').text(paintTool);
		var paintPrice  = jQuery(this).attr('data-priceTag');
		jQuery('.paintPrice').text(paintPrice);
 
	});
	

	// $('input[name="side_color"]').click(function() {
	// 	console.log("i have been clicked");
	// 	showLoader(); 
	// 	var imgFolder	= jQuery('#body-color').attr('data-folder'),
	// 		imgColor	= jQuery(this).attr('data-color'),
	// 		currentImg	= siteURL+'images/'+imgFolder+'/side-color/'+imgColor;
	// 		console.log(currentImg);
	// 		var image = new Image();
	// 		image.onload = function() {
	// 		  jQuery(".img-side-color").attr('src',currentImg);
	// 		  hideLoader();
	// 		};
	// 		image.src = currentImg;
	// 		$(".driveImg").attr('src', currentImg);
	// 	jQuery('.driveImg').attr('src',currentImg); 
	// 	console.log($(".driveImg"));
	// });
	
	jQuery('input[name="side_color"]').on('click',function(){
		showLoader(); 
		var imgFolder	= jQuery('#body-color').attr('data-folder'),
			imgColor	= jQuery(this).attr('data-color'),
			currentImg	= siteURL+'images/'+imgFolder+'/side-color/'+imgColor; 	
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-side-color").attr('src',currentImg);
			  hideLoader();
			};
			image.src = currentImg;
			console.log(image);
		jQuery('.driveImg').attr('src',currentImg); 
 
	});
	
	
	jQuery('input[name="color-2"]').on('click',function(){
		jQuery('.img-tire').attr('src',siteURL+'images/transparent.png');
		showLoader();
		var wheelsColor	= jQuery(this).attr('data-wheelsColor'),
			imgFolder	= jQuery('#body-color').attr('data-folder'),
			imgColor	= jQuery(this).attr('data-color'),
			currentImg	= siteURL+'images/'+imgFolder+'/rim/'+imgColor;
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-rim").attr('src',currentImg);
			  hideLoader();
			};
			image.src = currentImg; 	
			jQuery('.wheelsColor').text(wheelsColor); 
	});
	jQuery('input[name="modRim-color"]').on('click',function(){
		showLoader();
		var wheelNam 	= jQuery('input[name="color-4"]:checked').attr('data-wheelsname'),
		 	rimsColor	= jQuery(this).attr('data-rimsColor'),
			imgFolder	= jQuery('#body-color').attr('data-folder'),
			imgColor	= jQuery(this).attr('data-color'),
			currentImg	= siteURL+'images/'+imgFolder+'/rim/'+imgColor;
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-tire").attr('src',currentImg);
			  hideLoader();
			};
			image.src = currentImg;  	
			jQuery('.wheelsName').text(wheelNam+' ('+rimsColor+')');
			
	});
	jQuery('input[name="WB-tireColor"]').on('click',function(){
		showLoader();
		var wheelNam 	= jQuery('input[name="color-4"]:checked').attr('data-wheelsname'),
		 	rimsColor	= jQuery(this).attr('data-wheelscolor'),
			imgFolder	= jQuery('#body-color').attr('data-folder'),
			imgColor	= jQuery(this).attr('data-color'),
			currentImg	= siteURL+'images/'+imgFolder+'/tire/'+imgColor;
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-tire").attr('src',currentImg);
			  hideLoader();
			};
			image.src = currentImg;  	
			jQuery('.wheelsName').text(wheelNam+' ('+rimsColor+')');
	});
	jQuery('input[name="interior-color"]').on('click',function(){
		showLoader();
		var interiorColor	= jQuery(this).attr('data-intercolor'),
			imgFolder		= jQuery('#body-color').attr('data-folder'),
			imgColor		= jQuery(this).attr('data-color'),
			currentImg		= siteURL+'images/'+imgFolder+'/interior/'+imgColor;
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-interiorColor").attr('src',currentImg);
			  hideLoader();
			};
			image.src = currentImg;  	
			jQuery('.interiorColor').text(interiorColor);
	});
	jQuery('input[name="color-3"]').on('click',function(){
		showLoader();
		var imgFolder	= jQuery('#body-color').attr('data-folder'),
			imgColor	= jQuery(this).attr('data-color'),
			currentImg	= siteURL+'images/'+imgFolder+'/roof/'+imgColor; 
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-roof").attr('src',currentImg);
			  hideLoader();
			};
			image.src = currentImg; 
	});
	jQuery('input[name="color-4"]').on('click',function(){
		showLoader();
		var wheelsName  = jQuery(this).attr('data-wheelsName'),
		imgFolder		= jQuery('#body-color').attr('data-folder'),
			imgColor	= jQuery(this).attr('data-color'),
			currentImg	= siteURL+'images/'+imgFolder+'/tire/'+imgColor;
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-tire").attr('src',currentImg);
			  hideLoader();
			};
			image.src = currentImg; 
		jQuery('.wheelsName').text(wheelsName); 
	});
	jQuery('input[name="color-5"]').on('click',function(){
		showLoader();
		var stripeColor	= jQuery(this).attr('data-stripeColor'),
			imgFolder	= jQuery('#body-color').attr('data-folder'),
			imgColor	= jQuery(this).attr('data-color'),
			currentImg	= siteURL+'images/'+imgFolder+'/stripe/'+imgColor;
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-stripe").attr('src',currentImg);
			  hideLoader();
			};
			image.src = currentImg;  
		jQuery('.stripeColor').text(stripeColor); 
	});
	jQuery('input[name="color-7"]').on('click',function(){
		showLoader();
		var spoilerCheck = jQuery(this).attr('data-spoiler'),
			imgFolder	 = jQuery('#body-color').attr('data-folder'),
			imgColor	 = jQuery(this).attr('data-color'),
			currentImg	 = siteURL+'images/'+imgFolder+'/spoiler/'+imgColor;
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-spoiler").attr('src',currentImg);
			  hideLoader();
			};
			image.src = currentImg;  
		jQuery('.spoilerCheck').text(spoilerCheck); 
	});
	jQuery('input[name="color-9"]').on('click',function(){
		showLoader();
		var imgFolder	= jQuery('#body-color').attr('data-folder'),
			imgColor	= jQuery(this).attr('data-color'),
			currentImg	= siteURL+'images/'+imgFolder+'/interior/'+imgColor;
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-interior").attr('src',currentImg);
			  hideLoader();
			};
			image.src = currentImg;  
	});
	jQuery('input[name="color-10"]').on('click',function(){
		showLoader();
		var imgFolder	= jQuery('#body-color').attr('data-folder'),
			imgColor	= jQuery(this).attr('data-color'),
			currentImg	= siteURL+'images/'+imgFolder+'/moreRim/'+imgColor;
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-moreRim").attr('src',currentImg);
			  hideLoader();
			};
			image.src = currentImg; 
	});
	jQuery('input[name="color-11"]').on('click',function(){
		showLoader();
		var imgFolder	= jQuery('#body-color').attr('data-folder'),
			imgColor	= jQuery(this).attr('data-color'),
			currentImg	= siteURL+'images/'+imgFolder+'/sidePipes/'+imgColor;
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-sidePipes").attr('src',currentImg);
			  hideLoader();
			};
			image.src = currentImg; 
	});
	jQuery('input[name="color-12"]').on('click',function(){
		showLoader();
		var imgFolder		= jQuery('#body-color').attr('data-folder'),
			HoodScoopCheck  = jQuery(this).attr('data-hood-scoop'),
			imgColor		= jQuery(this).attr('data-color'),
			currentImg		= siteURL+'images/'+imgFolder+'/hoodScoope/'+imgColor;
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-hoodScoope").attr('src',currentImg);
			  hideLoader();
			};
			image.src = currentImg; 
		jQuery('.HoodScoopCheck').text(HoodScoopCheck); 
	});
 	jQuery('input[name="HL-type"]').on('click',function(){
		showLoader();
		var headlightsCheck = jQuery(this).attr('data-hlType'),
			imgFolder	 	= jQuery('#body-color').attr('data-folder'),
			imgColor	 	= jQuery(this).attr('data-color'),
			currentImg	 	= siteURL+'images/'+imgFolder+'/'+imgColor;
			var image = new Image();
			image.onload = function() {
			  jQuery(".img-HL-chrome").attr('src',currentImg).show();
			  hideLoader();
			};
			image.src = currentImg; 
		jQuery('.headlightsCheck').text(headlightsCheck);
		if (jQuery('#headlightColored').prop('checked')) {
			jQuery('.img-HL-chrome').attr('src',siteURL+'images/transparent.png').hide();
		} 
	});
	
	/* Car Images Folder Code Ends
	----------------------------*/
	
	/* Rim type Code Start
	----------------------*/
	jQuery('#tool_mod_rim').hide();
	jQuery('#tool_wb_tire').hide();
	jQuery('#modern').on('click',function(){
		jQuery('input#modRim-chrome').trigger('click');
		var wheelNam = jQuery(this).attr('data-wheelsname'),
			rimsColor	= jQuery('input[name="modRim-color"]:checked').attr('data-rimsColor');
		jQuery('#tool_rim').hide();
		jQuery('#tool_wb_tire').hide();
		jQuery('.img-rim').hide();
		jQuery('.img-tire').show();
		jQuery('#tool_mod_rim').show();
		//jQuery('.wheelsName').text(wheelNam+' ('+rimsColor+')');
	});
	jQuery('#stock').on('click',function(){
		jQuery('.img-tire').attr('src',siteURL+'images/transparent.png');
		jQuery('#tool_rim').show();
		jQuery('.img-rim').show();
		jQuery('#tool_mod_rim').hide();
		jQuery('#tool_wb_tire').hide();
		jQuery(".img-tire").attr('src',siteURL+'images/transparent.png');
	});
	jQuery('#mod-black').on('click',function(){
		jQuery('input#colorMBT').trigger('click');
		jQuery('#tool_rim').hide();
		jQuery('.img-rim').hide();
		jQuery('.img-tire').show();
		jQuery('#tool_mod_rim').hide();
		jQuery('#tool_wb_tire').show();
		jQuery(".img-tire").attr('src',siteURL+'images/transparent.png');
	});
	
	jQuery('#retroBlack').on('click',function(){
		jQuery('#tool_rim').hide();
	});
	
	/* Rim type Code Ends
	----------------------*/
	
});
 
},

    error: function() { // callback if there's an error
      alert("Ajax Error");
    }
 });
});
/* Ajax code END
------------------------*/

jQuery(document).ready(function(){
	"use strict";
	 
	/* Tabs Code Start
	----------------------*/
	jQuery('.tabLink').on('click',function(){
		var targetContent = jQuery(this).attr('data-target');
		if(jQuery(this).hasClass('active')){
			return false;
		}else{
			jQuery('.tabLink').removeClass('active');
			jQuery('.tabContent').hide();
			jQuery(this).addClass('active');
			jQuery(targetContent).show();
		}
	});
	/* Tabs Code Ends
	----------------------*/
	
	/* Pop-up Code Start
	----------------------*/
	jQuery('.openPop').on('click',function(){
		var itsModel = jQuery(this).attr('data-modal');
		jQuery(itsModel).addClass('showTime');
		jQuery('body').addClass('fixedBody'); 
	});
	jQuery('.overlay .close').on('click',function(){
		var itSModel = jQuery(this).parent().parent().parent();
		jQuery(itSModel).removeClass('showTime'); 
		jQuery('body').removeClass('fixedBody');
	});
	jQuery('.overlay').click(function() {
		jQuery(this).removeClass('showTime');
		jQuery('body').removeClass('fixedBody');
	});
	$('.customPopup').click(function(e) {
		e.stopPropagation(); 
	});
	/* Pop-up Code Ends
	----------------------*/
	
	/* order-Summary Code Start
	--------------------------*/
jQuery('#orderSummary').on('click',function(){
		 

	var	SlpaintPrice =      $('.paintPrice').text().replace(',',''),
		SlengPrice =        $('.engPrice').text().replace(',',''),
		SlbrakePricePrice = $('.brakePrice').text().replace(',',''),
		SlinterPrice =      $('.interPrice').text().replace(',',''),
		SlpriceTotal  = Number(SlpaintPrice)+Number(SlengPrice)+Number(SlbrakePricePrice)+Number(SlinterPrice);
		 
		jQuery('.optionAdded').text(SlpriceTotal);	// Total of Options
		
		
	var	basePrice   = jQuery('#b_price').text().replace(',',''), 
		leasePrice  = jQuery('#l_price').text().replace(',',''),
		ToptionAdded  = jQuery('.optionAdded').text().replace(',',''),
		
		priceTotal  = Number(basePrice)+Number(leasePrice)+Number(ToptionAdded),
		priceTotalOne= Number(basePrice)+Number(ToptionAdded);
		
		 
		if(jQuery('#check').prop("checked") === true){
			jQuery('#t_price').text(priceTotal);
		}
		else if(jQuery('#check').prop("checked") === false){
			jQuery('#t_price').text(priceTotalOne);
		}
		jQuery(".addCommas").each(function(){
			var num = jQuery(this).text();
			var commaNum = numberWithCommas(num);
			jQuery(this).text(commaNum);
		});
		
});
 
	/* Customize Now Code Start
	------------------------*/
	jQuery('.customizeNow').on('click',function(){ 
		jQuery('.wizard>.nav-wizard>li.active').removeClass('active');
		jQuery('.wizard>form>.tab-content>.tab-pane.active').removeClass('active');
		var imgFile	    = jQuery(this).closest('.carBigbox').attr('data-folder'),
			currentImg	= siteURL+'images/'+imgFile+'/black.jpg';
		jQuery(".img-change").attr('src',currentImg);
		jQuery('.driveImg').attr('src',currentImg);
		var baseLease     = jQuery(this).attr('data-lease');
		jQuery('.leasePrice').text(baseLease);
		var basePrice     = jQuery(this).attr('data-basePrice');
		jQuery('.carPrice').text(basePrice);
		jQuery('.carMainPrice').text(basePrice);
		jQuery('.bPrice').text(basePrice);
		jQuery('#leaseBind').hide();
		var titleCar = jQuery(this).attr('data-name'); 
		jQuery('.carTitle').text(titleCar);
		jQuery('#tool_paint').show();  
		jQuery('#tool_paint_gloss').hide();
		var customizeFor  = jQuery(this).closest('.carBigbox').attr('id'),
			currentFolder = jQuery('#'+customizeFor).attr('data-folder'),
			checkRoof	  = jQuery('#'+customizeFor).attr('data-roof'),
			checkTire     = jQuery('#'+customizeFor).attr('data-tire'),
			checkStripe   = jQuery('#'+customizeFor).attr('data-stripe'),
			checkBlacktire= jQuery('#'+customizeFor).attr('data-black-tire'),
			checkGloss    = jQuery('#'+customizeFor).attr('data-gloss'),
			checkSpoiler  = jQuery('#'+customizeFor).attr('data-spoiler'),
		 checkHeadlights  = jQuery('#'+customizeFor).attr('data-Headlights'),
	   checkInteriorColor = jQuery('#'+customizeFor).attr('data-Interior-Color'),   
		  checkInsetColor = jQuery('#'+customizeFor).attr('data-insetColor'),  
			checkColor    = jQuery('#'+customizeFor).attr('data-color-type'),
			checkInterior = jQuery('#'+customizeFor).attr('data-interior'),
			checkRim      = jQuery('#'+customizeFor).attr('data-rim'),
			checkmoreRim  = jQuery('#'+customizeFor).attr('data-moreRim'),
			checksidePipes= jQuery('#'+customizeFor).attr('data-side-pipes'), 
		   //customizAreaHi = jQuery('.customizer-container').height(),
			checkhoodScoop= jQuery('#'+customizeFor).attr('data-hood-scoop');
		jQuery('#'+customizeFor).removeClass('showTime'); 
		jQuery('body').removeClass('fixedBody');  // IMR
		jQuery('#body-color').attr('data-folder',currentFolder);
		jQuery('#customizerTab').parent().addClass('active');
		jQuery('#customizeArea').addClass('active');
 
 jQuery('#toolbox').height(662); 
		
		
		jQuery('#tool_rim').show();
		jQuery('.img-rim').show();
		jQuery('.img-rim,.img-roof,.img-tire,.img-stripe,.img-spoiler,.img-interior,.img-moreRim,.img-sidePipes,.img-hoodScoope').attr('src',siteURL+'images/transparent.png');
		if(checkRoof === 'no'){
			jQuery('#tool_roof').hide();
			jQuery('.img-roof').hide();
		}
		if(checkRoof === 'yes'){
			jQuery('#tool_roof').show();
			jQuery('.img-roof').show();
		}
		if(checkTire === 'no'){
			jQuery('#tool_tire').hide();
			jQuery('.img-tire').hide();
		}
		if(checkTire === 'yes'){
			jQuery('#tool_tire').show();
			jQuery('.img-tire').show();
			if(checkBlacktire === 'yes'){
				jQuery('#checkBlack').show();
			}
			if(checkBlacktire === 'no'){
				jQuery('#checkBlack').hide();
			}
		}
		if(checkStripe === 'no'){
			jQuery('#tool_stripe').hide();
			jQuery('.img-stripe').hide();
			jQuery('.itm-stripe').hide(); 
		}
		if(checkStripe === 'yes'){
			jQuery('#tool_stripe').show();
			jQuery('.img-stripe').show();
			jQuery('.itm-stripe').show();
		}
		if(checkGloss === 'no'){
			jQuery('#tool_paint_gloss').hide();
		}
		if(checkSpoiler === 'no'){
			jQuery('#tool_spoiler').hide();
			jQuery('.img-spoiler').hide();
		}
		if(checkSpoiler === 'yes'){
			jQuery('#tool_spoiler').show();
			jQuery('.img-spoiler').show();
		}
		if(checkColor === 'no'){
			jQuery('#tool_color_type').hide();
			jQuery('#tool_paint').show();
		}
		if(checkColor === 'yes'){
			jQuery('#tool_color_type').show();
		}
		if(checkInterior === 'no'){
			jQuery('#tool_interior').hide();
			jQuery('.img-interior').hide();
		}
		if(checkInterior === 'yes'){
			jQuery('#tool_interior').show();
			jQuery('.img-interior').show();
		}
		if(checkRim === 'no'){
			jQuery('#tool_rim').hide();
		}
		if(checkmoreRim === 'no'){
			jQuery('#tool_more_rim').hide();
			jQuery('.img-moreRim').hide();
		}
		if(checkmoreRim === 'yes'){
			jQuery('#tool_more_rim').show();
			jQuery('.img-moreRim').show();
		}
		if(checksidePipes === 'no'){
			jQuery('#tool_side_pipes').hide();
			jQuery('.img-sidePipes').hide();
		}
		if(checksidePipes === 'yes'){
			jQuery('#tool_side_pipes').show();
			jQuery('.img-sidePipes').show();
		}
		if(checkhoodScoop === 'no'){
			jQuery('#tool_hood_scoop').hide();
			jQuery('.img-hoodScoope').hide();
		}
		if(checkhoodScoop === 'yes'){
			jQuery('#tool_hood_scoop').show();
			jQuery('.img-hoodScoope').show();
		}
		if(checkHeadlights === 'no'){  
			jQuery('.img-HL-chrome').attr('src',siteURL+'images/transparent.png').hide();
			jQuery('.itm-hl-type').hide(); 
		}
		if(checkHeadlights === 'yes'){ 
			jQuery('.img-HL-chrome').show();
			jQuery('.itm-hl-type').show(); 
		} 
		if(checkInteriorColor === 'no'){ 
			jQuery('.img-interiorColor').hide();
			jQuery('.itm-inter-color').hide(); 
			
		}
		if(checkInteriorColor === 'yes'){ 
			jQuery('.img-interiorColor').show();
			jQuery('.itm-inter-color').show();  
		}
		
		
		if(checkInsetColor === 'no'){
			jQuery('.img-tire').attr('src',siteURL+'images/transparent.png'); 
			jQuery('.img-tire').hide(); 
		}
		if(checkInsetColor === 'yes'){ 
			jQuery('.img-tire').show();
		}
		
		var targitElemnt = jQuery(this).closest('.carBigbox');
		jQuery('.img-side-color').remove();
		if(jQuery(targitElemnt).attr('data-sideColor')){ 
			jQuery('.customizer-container, .drive-in').append('<img class="img-side-color" src="'+siteURL+'images/transparent.png">');
		}
	jQuery('.img-interiorColor').attr('src',siteURL+'images/transparent.png');
	});
	

	/* Customize Now Code Ends
	-------------------------*/
	
	
	
	/* Hamburger_icon Code Start
	--------------------------*/
		jQuery(".header_container").on('click', '.top', function() {
			jQuery('.links_responsive').toggleClass("toggle");
		});
		
		jQuery('.closeMenu').click(function() {
			jQuery('.links_responsive').removeClass("toggle");
		});
	/* Hamburger_icon Code Ends
	--------------------------*/
 

// Category Tabs
 
	"use strict";
	jQuery("div.category-tab-menu>div.list-group>a").click(function(e){
		e.preventDefault();
		jQuery(this).siblings('a.active').removeClass("active");
		jQuery(this).addClass("active");
		var index = jQuery(this).index();
		jQuery("div.category-tab>div.category-tab-content").removeClass("active");
		jQuery("div.category-tab>div.category-tab-content").eq(index).addClass("active");
	});
 
		
});
		


 
/* Shopping Cart Code Starts
------------------------------*/
 jQuery('.minus-btn').on('click', function(e) {
	e.preventDefault();
	var name = jQuery(this);
	var input = name.closest('div').find('input');
	var value = parseInt(input.val());

	if (value > 1) {
		value = value - 1;
	} else {
		value = 0;
	}

input.val(value);

});

jQuery('.plus-btn').on('click', function(e) {
	e.preventDefault();
	var name = jQuery(this);
	var input = name.closest('div').find('input');
	var value = parseInt(input.val());

	if (value < 100) {
	value = value + 1;
	} else {
		value =100;
	}

	input.val(value);
});

jQuery('.like-btn').on('click', function() {
	jQuery(this).toggleClass('is-active');
});
jQuery('#d-btn1').on('click', function() {
	jQuery('.list-1').hide();
});
jQuery('#d-btn2').on('click', function() {
	jQuery('.list-2').hide();
});
jQuery('#d-btn3').on('click', function() {
	jQuery('.list-3').hide();
});
/* Shopping Cart Code Ends
------------------------------*/


/* Custom Code IMR Start
---------------------------------*/ 
 
jQuery(document).ready(function(){
  
if (jQuery('.ex-SelctedOptions').children().length < 2 ) {
	 jQuery('.ex-SelctedOptions').prepend('<li class="no-options">No any option is selected.</li>'); 
} 

jQuery('#orderSummary').click(function(){ 
	if ( jQuery('.ex-SelctedOptions').children().length < 2 ) {
		 jQuery('.ex-SelctedOptions').prepend('<li class="no-options">No any option is selected.</li>'); 
	}  
	var brakePrice_  = jQuery('input[name="brakes-type"]:checked').prev('strong').text();
	jQuery('.brakePrice').text(brakePrice_); 
	 
	/* Lease CheckBox Start
	-----------------------*/ 
	if(jQuery('div.lecense>input[type="checkbox"]').prop('checked')){
		jQuery('#leaseBind').show();
	} else {
		jQuery('#leaseBind').hide();
	} 
	/* Lease CheckBox Code Ends
	-------------------------*/  
	if (jQuery('#mod-black').prop('checked')) {
		jQuery('.itm-wh-color').hide();
	}  
	else if (jQuery('#modern').prop('checked')) {
		jQuery('.itm-wh-color').hide();
	}
	else {
		jQuery('.itm-wh-color').show();
	}

});


jQuery('.lecense').click(function() {
	jQuery(this).children('#check').trigger('click');
});
 
jQuery('#submitOrder').on('click',function(){
	jQuery('#popup2').removeClass('showTime'); 
});
 
defaultSettings();
jQuery(document).on('click','.wizard>.nav-wizard>li>a[data-toggle="tab"]', function(){  
	defaultSettings();  
});

res_accordionFun(); 
 

 
});


 	
jQuery(window).resize(function(){
	var customizAreaHi = jQuery('.customizer-container').height(); 
	jQuery('#toolbox').height(customizAreaHi); 
});

/* Comma Code Start
-------------------------*/
function numberWithCommas(number){
	"use strict";
	var parts = number.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}
/* Comma Code Ends
-------------------------*/

function res_accordionFun() {
	if (jQuery(window).width() < 768 ) {	
		var allPanels = jQuery('.category-tab > .category-tab-content ').hide();
		jQuery('.category-tab .active').show();  
		jQuery('.category-tab > .accordion').click(function() {
			allPanels.slideUp();  
			//jQuery(this).next().slideDown(); 
			if(jQuery(this).next().is(":visible")) {
				jQuery(this).next().slideUp();
			}
			else {
				jQuery(this).next().slideDown();
			} 
			return false;
		});  
	}
 
}  
function defaultSettings() {
	jQuery('input[name="color"], input[name="color-2"], input[name="color-4"], input[name="color-5"], input[name="color-7"], input[name="color-8"], input[name="color-11"], input[name="color-12"]').prop('checked',false);
	jQuery('input#color, input#color31, input#color101, input#color913, input#stock, input#noSpoiler, input#noHood, input#stockColor, input#noExhaust').prop('checked',true);
	jQuery('input#color01').prop('checked',true); 
	jQuery('.paintName').text('Shadow Black');
	
}


// Form Validation Code Start
$(document).ready(function () {
// Form 1 Code Start
  $('#submitOrder').click(function (e) { 
  	if($.trim($('#nameFld').val()) == ''){
		$('#nameFld').addClass('red_hilighted');
		$('.msgF1').show();
	}
	else if($.trim($('#emailFld').val()) == ''){
		$('#emailFld').addClass('red_hilighted');
		$('.msgF1').show();
	}
	else if($.trim($('#phFld').val()) == ''){
		$('#phFld').addClass('red_hilighted');
		$('.msgF1').show();
	}
	else {
		$.ajax({
		method: 'POST',
		url: 'http://production.technology-architects.com/design/car-customizer/sendemail.php',
		data: $('#Form1').serialize(),
		datatype: 'json'
	});
		$('.successPopBtn').trigger('click');
		$('.msgF1').hide(); 
		$('#nameFld,#emailFld,#phFld').removeClass('red_hilighted').val('');
		setTimeout(function(){
			$('#successPopup,#popup1').removeClass('showTime');
			jQuery('body').removeClass('fixedBody');
		},3000);
	}        
	});
	
// Form 2 Code Start
  $('#getPriceBtn').click(function (e) { 
  	if($.trim($('#fName').val()) == ''){
		$('#fName').addClass('red_hilighted');
		$('.msgF2').show();
	}
	else if($.trim($('#lName').val()) == ''){
		$('#lName').addClass('red_hilighted');
		$('.msgF2').show();
	}
	else if($.trim($('#emailadd').val()) == ''){
		$('#emailadd').addClass('red_hilighted');
		$('.msgF2').show();
	}
	else if($.trim($('#phonenmbr').val()) == ''){
		$('#phonenmbr').addClass('red_hilighted');
		$('.msgF2').show();
	} 
	else if($.trim($('#zipcord').val()) == ''){
		$('#zipcord').addClass('red_hilighted');
		$('.msgF2').show();
	}
	else {
		$('.successPopBtn').trigger('click');
		$('.msgF2').hide(); 
		$('#fName,#lName,#emailadd,#phonenmbr,#zipcord').removeClass('red_hilighted').val('');
		setTimeout(function(){
			$('#successPopup,#popup2').removeClass('showTime');
			jQuery('body').removeClass('fixedBody');
		},3000); 
	}        
	});

});
 
function showLoader() {
	jQuery('.loading').show();  	
}
function hideLoader() {  
	jQuery('.loading').hide();
}

/* Custom Code IMR Ends
------------------------------*/
//capture image , save image and send to email client/admin
$(document).ready(function() {
$("#submit").click(function() {
	
$([document.documentElement, document.body]).animate({
scrollTop: $('.customizer-container').offset().top }, 0);
var name = $("#name").val();
var email = $("#email").val();
var contact = $("#contact").val();
contact = contact.replace(/[^0-10]/g,'');
var cartitle = $(".carTitle").html();
var paint = $(".paintName").html();
var wheelsName = $(".wheelsName").html();
var wheelsColor = $(".wheelsColor").html();
var stripeColor = $(".stripeColor").html();
var spoilerCheck = $(".spoilerCheck").html();
var engName   = $(".engName").html();
var brakeName   = $(".brakeName").html();
var interName   = $(".interName").html();
var ModernSoundSystem   = $(".Mod-Sound-System-ex-c").html();
var HeatedSeats   = $(".Heated-Seats-ex-c").html();
var PaddleShift   = $(".Paddle-Shift-ex-c").html();
var DigitalDash  = $(".Digital-Dash-ex-c").html();
var AutoTrunk  = $(".Auto-Trunk-ex-c").html();
var PowerLocks  = $(".Power-Locks-ss-ex-c").html();
var KeylessPowerLocks  = $(".Power-Locks-ss-ex-c").html();
var KeylessPowerLocks2  = $(".Power-Locks-ex-c").html();
var PowerWindows  = $(".Power-Windows-ex-c").html();
var ClimateControl  = $(".Climate-Control-ex-c").html();
var PowerSeats  = $(".Power-Seats-ex-c").html();
var MediaNavSystem  = $(".Media-Nav-System-ex-c").html();
var contact = $("#contact").val();
var message = $("#message").val();
alert(message);
//alert(cartitle);
$("#returnmessage").empty(); // To empty previous error/success message. 
$('.msgF3').hide();
$('#name,#email,#contact').removeClass('red_hilighted');
 
if($.trim($('#name').val()) == ''){
	$('#name').addClass('red_hilighted');
	$('.loading').hide();
	$('.msgF3').show().html('<p>Please Enter the Name.</p>');
}
else if($.trim($('#email').val()) == ''){
	$('#email').addClass('red_hilighted');
	$('.loading').hide();
	$('.msgF3').show().html('<p>Please Add Email Address.</p>');
}
else if($.trim($('#contact').val()) == ''){
	$('#contact').addClass('red_hilighted');
	$('.loading').hide();
	$('.msgF3').show().html('<p>Please Add Phone Number.</p>');
}
else if (contact.length != 11) {
	$('#contact').addClass('red_hilighted');
	$('.loading').hide();
	$('.msgF3').show().html('<p>Phone number must be 11 digits.</p>');   
}
else if(IsEmail(email)==false){
  $('#email').addClass('red_hilighted');
  $('.msgF3').show().html('<p>Invalid email Address.</p>');
  return false;
}

else {
$('.loading').show(); 
$('.successPopBtn').trigger('click');
html2canvas($(".customizer-container"), { 
            onrendered: function (canvas) { 
                var imageStr2 = canvas.toDataURL('png');
                var rawData2 = canvas.toDataURL("image/png");
				rawData2 = rawData2.substr("data:image/png;base64,".length);
                var img_name = 'car';
                var datasend = 'car';
				console.log(imageStr2);
                jQuery.ajax({

                    type: 'POST',
                    url: siteURL+"base64_image.php",
                    data: {

                        action: 'canvas_funtion',
                        image: imageStr2,
                        img_name: img_name
                    },
                    //dataType: 'json',
                    encode: true,
                    success: function (image_name) {
						console.log(image_name);
                        //var image_id = image_name;
						var image_url = siteURL+'uploads/'+image_name;
						console.log(image_url);
						// Returns successful data submission message when the entered information is stored in database.
						$.post("//www.revo-motors.com/wp-content/themes/Avada/sendemail.php", {
						name1: name,
						email1: email,
						model: cartitle,
						paint: paint,
						wheelsName: wheelsName,
						wheelsColor: wheelsColor,
						stripeColor: stripeColor,
						spoilerCheck: spoilerCheck,
						engName:engName,
						brakeName:brakeName,
						interName:interName,
						ModernSoundSystem:ModernSoundSystem,
						HeatedSeats:HeatedSeats,
						PaddleShift:PaddleShift,
						DigitalDash:DigitalDash,
						AutoTrunk:AutoTrunk,
						PowerLocks:PowerLocks,
						KeylessPowerLocks:KeylessPowerLocks,
						KeylessPowerLocks2:KeylessPowerLocks2,
						PowerWindows:PowerWindows,
						ClimateControl:ClimateControl,
						PowerSeats:PowerSeats,
						MediaNavSystem:MediaNavSystem,
						contact1: contact,
						message: "Here is a message",
						image_url:image_url
						}, function(data) {
						$('.loading').hide();
								$('.msgF2').hide(); 
								$('#name,#email,#contact,#fName,#lName,#emailadd,#phonenmbr,#zipcord').removeClass('red_hilighted').val('');
								setTimeout(function(){
									$('#successPopup,#popup1').removeClass('showTime');
									$('html').removeClass('fixedBody');
									$('.successMsg').remove();
								},3000);
						$("#returnmessage").append(data); // Append returned message to message paragraph.
						console.log(data);//alert(data);
						if (data == "11") {
						//$("#form")[0].reset(); // To reset form fields on success.
						//alert(data);
						}
						});
                    }
                });
            },
            logging: true,
 		});	
	  }
	});
});

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(email)) {
    return false;
  }else{
    return true;
  }
}