<?php
// Fetching Values from URL.
date_default_timezone_set('America/Argentina/Buenos_Aires');
$name 				= $_POST['name1'];
$email 				= $_POST['email1'];
$model  			= $_POST['model'];
$paint 				= $_POST['paint'];
$wheelsName 		= $_POST['wheelsName'];
$wheelsColor 		= $_POST['wheelsColor'];
$stripeColor 		= $_POST['stripeColor'];
$spoilerCheck 		= $_POST['spoilerCheck'];
$engName 			= $_POST['engName'];
$brakeName 			= $_POST['brakeName'];
$interName 			= $_POST['interName'];
$ModernSoundSystem  = $_POST['ModernSoundSystem'];
$HeatedSeats 		= $_POST['HeatedSeats'];
$PaddleShift 		= $_POST['PaddleShift'];
$DigitalDash 		= $_POST['DigitalDash'];
$AutoTrunk 			= $_POST['AutoTrunk'];
$PowerLocks 		= $_POST['PowerLocks'];
$KeylessPowerLocks  = $_POST['KeylessPowerLocks'];
$KeylessPowerLocks2 = $_POST['KeylessPowerLocks2'];
$PowerWindows 		= $_POST['PowerWindows'];
$ClimateControl 	= $_POST['ClimateControl'];
$PowerSeats 		= $_POST['PowerSeats'];
$MediaNavSystem 	= $_POST['MediaNavSystem'];
$contact 			= $_POST['contact1'];
$message            = $_POST['message'];
$image_url 			= $_POST['image_url'];
$img_url = str_replace('//www', 'www', $image_url);

$email = filter_var($email, FILTER_SANITIZE_EMAIL); // Sanitizing E-mail.
// After sanitization Validation is performed
if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
 

$template = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>Order Summary</title><meta name="format-detection" content="telephone=no" /><meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" /><style>a[x-apple-data-detectors] {color:inherit !important;}</style></head><body style="background:#4d4d4d;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;color:#353535;padding:0;margin:0 auto;">

<table class="tblWrap" style="width:600px;margin:0 auto;background:#202020;"><tr><td style="padding:0">
<table class="header" style="width: 100%;border-collapse:collapse;" >
<tr><td style="border: none;padding:30px 15px;font-size:22px;text-transform:uppercase;letter-spacing:1px;text-decoration:none;color:#fff;font-weight:700;"><a href="#" style="font-size:22px;text-transform:uppercase;letter-spacing:1px;text-decoration:none;color:#fff;font-weight:700;">Revolution Motors</a></td>
<th align="right" style="border: none;padding:30px 15px;font-size:14px;color: #fff;">Phone: (513) 772-4105</th>
</tr>
</table>
 <table class="header" style="width: 100%;border-collapse:collapse;" >
<tr>
<td>
<th style="text-align:left;padding:4px 15px 10px;vertical-align: middle;color:#a4a4a4;">You Have an order with following details.</th>
</td>
</tr>
</table> 
<table class="header" style="width: 100%;border-collapse:collapse;" >
<tr><td style="padding:4px 15px;vertical-align: middle;color:#a4a4a4;">Name: '. $name .'</td><td style="padding:4px 15px;vertical-align: middle;color:#a4a4a4;">Email: <a href="mailto:' . $email . '" style="color:#a4a4a4;">' . $email . '</a></td></tr>
<tr><td style="padding:4px 15px;vertical-align: middle;color:#a4a4a4;">Phone: ' . $contact . '</td><td style="padding:4px 15px;vertical-align: middle;color:#a4a4a4;">Date:'.date('M-D-Y').' </td></tr><tr><th align="left" style="padding:8px 15px;vertical-align:middle;line-height:16px;color:#a4a4a4;"><b>Car: '.$model.'</b></th><th></th></tr>
<tr><td style="padding:4px 15px;vertical-align: middle;color:#a4a4a4;">Custom Extras: '. $message .'</td></tr>
</table>

<table style="width:100%;border-collapse:collapse;" cellpadding="0" cellspacing="0" border="0">
<tr><td style="padding:10px 15px 30px">


<table class="header" style="width: 100%;border-collapse:collapse;" >
<tr>
<th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border: 1px solid #666;width:20%">Model</th>
<th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border: 1px solid #666;">'.$model.'</th>
</tr>
</table>
 
<table style="width:100%;border-collapse:collapse;">
<tr><td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;height:310px;"><img src="'. $img_url .'" height="300" width="400" /></td></tr>
<tr><td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">If you dont see image in email please <a href="'. $img_url .'" target="_blank">click here</a> to view the image in your browser</td></tr>


<tr><th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">Paint and Exterior</th></tr>

<tr><td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">Paint: '.$paint.'</td></tr>
<tr><td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">Wheels Name: '.$wheelsName.'</td></tr>
<tr><td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">Wheels Color: '.$wheelsColor.'</td></tr>
<tr><td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">Stripe Color: '.$stripeColor.'</td></tr>
<tr><td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">'.$spoilerCheck.'</td></tr>
 
<tr><th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">PowerTrain</th></tr>
<tr><td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">'.$engName.'</td></tr>

<tr><th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">Brakes</th></tr>
<tr> <td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">'.$brakeName.'</td> </tr>

<tr><th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">Interior</th></tr>
<tr><td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">'.$interName.'</td></tr>

<tr><th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">Extras</th></tr>
<tr><td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">
';

if($ModernSoundSystem !="")
$template .=''.$ModernSoundSystem.'<br />';
if($HeatedSeats !="")
$template .=''.$HeatedSeats.'<br />';
if($PaddleShift !="")
$template .=''.$PaddleShift.'<br />';
if($DigitalDash !="")
$template .=''.$DigitalDash.'<br />';
if($AutoTrunk !="")
$template .=''.$AutoTrunk.'<br />';
if($PowerLocks !="")
$template .=''.$PowerLocks.'<br />';
if($KeylessPowerLocks !="")
$template .=''.$KeylessPowerLocks.'<br />';
if($KeylessPowerLocks2 !="")
$template .=''.$KeylessPowerLocks2.'<br />';
if($PowerWindows !="")
$template .=''.$PowerWindows.'<br />';
if($ClimateControl !="")
$template .=''.$ClimateControl.'<br />';
if($PowerSeats !="")
$template .=''.$PowerSeats.'<br />';
if($MediaNavSystem !="")
$template .=''.$MediaNavSystem.'<br />';



$template .='<tr><th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">Packages</th></tr><tr> <td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;"></td> </tr></table> </td></tr></table><table class="footer" style="width: 100%;border-collapse: collapse;border-top: 1px solid #333;"><tr><td style="padding:24px 15px 10px" ><table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td style="padding-bottom:0px;color:#797979;font-size:11px;line-height:10px;text-align:center;letter-spacing:1px">Revolution Motors, 11741 Chesterdale Road, Cincinnati, Ohio 45246<br><br></td></tr><tr><td style="padding-bottom:13px;color:#797979;font-family:Helvetica,Arial,sans-serif;font-size:11px;line-height:16px;text-align:center"><a href="#" style="color:#979696;text-decoration:underline" target="_blank"><span style="color:#979696;text-decoration:underline">Terms of Service</span></a> &nbsp;| &nbsp;<a href="#" style="color:#979696;text-decoration:underline" target="_blank"><span style="color:#979696;text-decoration:underline">Privacy Policy</span></a> &nbsp;| &nbsp;<a href="#" class="m_-6010444798861659531link-grey-u" style="color:#979696;text-decoration:underline" target="_blank" data-saferedirecturl="#"><span style="color:#979696;text-decoration:underline">Unsubscribe</span></a></td></tr><tr></tr></tbody></table></td></tr></table></td></tr></table></body></html>';

 
$sendmessage =   $template ;
// Message lines should not exceed 70 characters (PHP rule), so wrap it.
$sendmessage = wordwrap($sendmessage, 70);
// Send mail by PHP Mail Function.
$subject = "Revo-motors Contact Us Form";
//$CCemail = 'i.ahmed@technology-architects.com';
// To send HTML mail, the Content-type header must be set.
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: Revolution Motors <contact@revo-motors.com>'. "\n"; // Sender's Email
//$headers .= 'Cc:' . $CCemail. "rn"; // Carbon copy to Sender
//$headers .= 'Cc:' . $email. "rn"; // Carbon copy to Sender
mail("contact@revo-motors.com", $subject, $sendmessage, $headers);
echo "1";
} else {
echo "0";
}


//email body for client
$template_user = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Build Summary</title>
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
   <style>
   a[x-apple-data-detectors] {  color: inherit !important; }
   </style>
</head>
<body style="background:#4d4d4d;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;color:#353535;padding:0;margin:0 auto;">
<table class="tblWrap" style="width:600px;margin:0 auto;background:#202020;border-collapse:collapse;border-spacing:0;">
<tr>
<td style="padding:0">
<table class="header" style="width:100%;border-spacing:0;" >
<tr>
    <td style="border: none;padding:30px 15px;font-size:22px;text-transform:uppercase;letter-spacing:1px;text-decoration:none;color:#fff;font-weight:700;">
    <a href="#" style="font-size:22px;text-transform:uppercase;letter-spacing:1px;text-decoration:none;color:#fff;font-weight:700;">Revolution Motors</a>
    </td>
    <th align="right" style="border: none;padding:30px 15px;font-size:14px;color: #fff;">Phone: (513) 772-4105</th>
</tr> 
<tr>
<th style="text-align:left;padding:4px 15px;vertical-align: middle;color:#a4a4a4;">Your Build Summary</th>
<td align="right" style="padding:4px 15px;vertical-align: middle;color:#a4a4a4;">Date:'.date('M-D-Y').'</td>
</tr>
</table>
<table style="width:100%;border-spacing:0;">
<tr><th style="padding:8px 15px 0;vertical-align:middle;text-align:left;line-height:16px;color:#a4a4a4;"><b>'.$model.'</b></th></tr>
	<!--tr><td style="padding:8px 15px;vertical-align:middle;font-size:12px; line-height:16px;color:#a4a4a4;">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.
    </td></tr--> 
<tr><td style="padding:4px 15px;vertical-align: middle;color:#a4a4a4;">Custom Extras: '. $message .'</td></tr>
</table> 
<table style="width:100%;border-spacing:0;border-collapse:collapse;">
  <tr>
    <td style="padding:10px 15px;">
        <table style="width:100%;border-spacing:0;" cellpadding="0" cellspacing="0">
        <tr>
        <td> 
		<table style="width:100%;border-spacing:0;">
<tr>
<th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;width:20%">Model</th>
<td style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">'.$model.'</td> 
</tr>
            </table>
			<table style="width:100%;border-spacing:0;">
<tr><td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;height:310px;"><img src="'. $img_url .'" height="300" width="400" /></td></tr>
<tr><td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">If you dont see image in email please <a href="'. $img_url .'" target="_blank">click here</a> to view the image in your browser</td></tr>
</td></tr> 
            </table>
        </td> 
        </tr>
        <tr>
        	<th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">Paint and Exterior</th>
        </tr>
        <tr>
        <td style="padding:0;">
            <table style="width:100%;border-spacing:0;border-collapse:collapse;">
                <tr>
                	<td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">'.$paint.'</td>
                    <!--<td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;width:25%" align="center">$100</td>-->
                </tr>
                <tr>
                    <td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">'.$wheelsName.'<br>'.$wheelsColor.'</td>
                    <!--<td align="center" style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;width:25%">$100</td>-->
                </tr>
                <tr>
                    <td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">'.$stripeColor.'</td>
                   <!--<td align="center" style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;width:25%">$100</td>-->
                </tr>
				<tr><td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">'.$spoilerCheck.'</td></tr>
            </table>
        </td> 
        </tr>
        <tr>
        	<th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">PowerTrain</th>
        </tr>
        <tr>
        <td style="padding:0;">
            <table style="width:100%;border-spacing:0;" cellpadding="0" cellspacing="0" border="0">
                <tr> 
                    <td style="border:1px solid #666;border-right:0;padding:8px 15px;vertical-align:middle;color:#fff;">'.$engName.'</td>
                    <!--<td align="center" style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;width:25%">$0</td>-->
                </tr>
            </table>
        </td> 
        </tr>
        <tr>
        	<th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">Brakes</th>
        </tr>
        <tr>
        <td style="padding:0;">
            <table style="width:100%;border-spacing:0;" cellpadding="0" cellspacing="0" border="0">
                <tr> 
                    <td style="border:1px solid #666;border-right:0;padding:8px 15px;vertical-align:middle;color:#fff;">'.$brakeName.'</td>
                    <!--<td align="center" style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;width:25%">$0</td>-->
                </tr>
            </table>
        </td> 
        </tr>
        <tr>
        	<th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">Interior</th>
        </tr>
        <tr>
        <td style="padding:0;">
            <table style="width:100%;border-spacing:0;">
                <tr>
                    <td style="border:1px solid #666;border-right:0;padding:8px 15px;vertical-align:middle;color:#fff;">'.$interName.'</td>
                    <!--<td align="center" style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;width:25%">$0</td>-->
                </tr>
            </table>
        </td> 
        </tr>
        <tr>
        	<th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">Extras</th>
        </tr><tr><td style="color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">';
if($ModernSoundSystem !="")
$template_user .=''.$ModernSoundSystem.'<br/>';
if($HeatedSeats !="")
$template_user .=''.$HeatedSeats.'<br />';
if($PaddleShift !="")
$template_user .=''.$PaddleShift.'<br />';
if($DigitalDash !="")
$template_user .=''.$DigitalDash.'<br />';
if($AutoTrunk !="")
$template_user .=''.$AutoTrunk.'<br />';
if($PowerLocks !="")
$template_user .=''.$PowerLocks.'<br  />';
if($KeylessPowerLocks !="")
$template_user .=''.$KeylessPowerLocks.'<br />';
if($KeylessPowerLocks2 !="")
$template_user .=''.$KeylessPowerLocks2.'<br />';
if($PowerWindows !="")
$template_user .=''.$PowerWindows.'<br />';
if($ClimateControl !="")
$template_user .=''.$ClimateControl.'<br />';
if($PowerSeats !="")
$template_user .=''.$PowerSeats.'<br />';
if($MediaNavSystem !="")
$template_user .=''.$MediaNavSystem.'<br />';
$template_user .='<tr>
        	<th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">Packages</th>
        </tr>
        <tr>
        <td>
            <table style="width:100%;border-spacing:0;" cellpadding="0" cellpadding="0"> 
                <tr style="border:0;"> 
                    <td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;"></td>
                    <!--<td align="center" style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;width:25%">$100</td>-->
                </tr> 
            </table>
        </td> 
        </tr>
        </table> 
   <!--      
        <table style="width:63%;border-spacing:0;float:right;" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td style="padding:10px 0 10px">
            <table style="width:100%;border-spacing:0;" cellpadding="0" cellspacing="0" border="0">  
            <tr>
            <th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">Payment</th>
            </tr>
            <tr>
            <td style="padding:0;">
                <table style="width:100%;border-spacing:0;border-collapse:collapse" cellpadding="0" cellspacing="0" border="0">
                    <tr> 
                        <td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">Base Price</td>
                   <td align="center" style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;width:40%">$22,000</td>
                    </tr>
                    <tr>
                    <td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">Total of Options</td>
                    <!--<td align="center" style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;width:40%">$1650</td>
                    </tr>
                </table>-->
            </td> 
            </tr> 
             <!-- <tr><th style="background:#4d4d4d;color:#fff;text-align:left;padding:4px 15px;vertical-align: middle;border-left:1px solid #666;border-right:1px solid #666;">Total Price Altogether</th></tr>
            <tr>
            <td style="padding:0;">
               <table style="width:100%;border-spacing:0;border-collapse:collapse" cellpadding="0" cellspacing="0" border="0">
                    <tr> 
                    <td style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;">Estimated Net Price</td>
                    <td align="center" style="border:1px solid #666;padding:8px 15px;vertical-align:middle;color:#fff;width:40%">$23,650</td>
                    </tr> 
                </table>
            </td> 
            </tr> -->
			
			<tr>
			<td>
			<table style="width:100%;border-spacing:0;border-top:1px solid #333;">
        <tr>
            <td style="padding:24px 15px 10px" >
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td style="padding-bottom:0px;color:#797979;font-size:11px;line-height:10px;text-align:center;letter-spacing:1px">
                            Revolution Motors, 11741 Chesterdale Road, Cincinnati, Ohio 45246<br><br>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom:13px;color:#797979;font-family:Helvetica,Arial,sans-serif;font-size:11px;line-height:16px;text-align:center">
                            <a href="#" style="color:#979696;text-decoration:underline" target="_blank"><span style="color:#979696;text-decoration:underline">Terms of Service</span>
                            </a> &nbsp;| &nbsp;<a href="#" style="color:#979696;text-decoration:underline" target="_blank"><span style="color:#979696;text-decoration:underline">Privacy Policy</span></a> &nbsp;| &nbsp;<a href="#" class="m_-6010444798861659531link-grey-u" style="color:#979696;text-decoration:underline" target="_blank" data-saferedirecturl="#"><span style="color:#979696;text-decoration:underline">Unsubscribe</span></a>
                            </td>
                        </tr>
                        <tr> 
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
</table>
			</td>
			</tr>
            </table>
            </td>
        </tr>
        </table> 
    </td>
  </tr> 
</table> 

</td> 
</tr>
</table> 
</body>
</html>
';

$email = filter_var($email, FILTER_SANITIZE_EMAIL); // Sanitizing E-mail.
// After sanitization Validation is performed
if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
 
$subject = "Thanks for Contacting Revolution Motors";
//$CCemail = 'i.ahmed@technology-architects.com';
// To send HTML mail, the Content-type header must be set.
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: Revolution Motors <contact@revo-motors.com>'. "\n"; //Sender's Email
$sendmessage =   $template_user ;
// Message lines should not exceed 70 characters (PHP rule), so wrap it.
$sendmessage = wordwrap($sendmessage, 70);
// Send mail by PHP Mail Function.
mail($email, $subject, $sendmessage, $headers);
echo "1";
 
} else {
echo "0";
}
exit;
?>