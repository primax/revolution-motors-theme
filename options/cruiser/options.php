<div class="panel panel-default">
  <div class="panel-heading open">
    <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="panel-title expand">
      <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
      <a href="javascript:void(0);"><i class="fa fa-paint-brush"></i>Paint & Exterior</a> </h4>
  </div>
  <div id="collapse1" class="panel-collapse collapse in">
    <div class="tool-bar">
      <div class="tab-content">
        <div class="tab-pane active" id="body-color" data-folder="cruiser">
           
          <div class="bodyColor" id="tool_paint">
            <h3 class="heading-color">Matte Paint</h3>
            <div class="colorBox">
              <input type="radio" id="color01" name="color" checked="checked"  data-color="black.jpg" data-colorName="Shadow black" data-priceTag="0">
              <label title="black" for="color01"> <i class="Shadow-Black"></i> </label>
              <p class="color-title">Shadow Black</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color03" name="color" data-color="white.jpg" data-colorName="Oxford white" data-priceTag="0">
              <label title="white" for="color03"> <i class="Shadow-white"></i> </label>
              <p class="color-title">Oxford white</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color07" name="color" data-color="silver.jpg" data-colorName="Ingot Silver" data-priceTag="0">
              <label title="silver" for="color07"> <i class="Shadow-silver"></i> </label>
              <p class="color-title">Ingot Silver</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color06" name="color" data-color="red.jpg" data-colorName="Red" data-priceTag="0">
              <label title="red" for="color06"> <i class="Shadow-red"></i> </label>
              <p class="color-title"> Red</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color08" name="color" data-color="yellow.jpg" data-colorName="Light Yellow" data-priceTag="0">
              <label title="yellow" for="color08"> <i class="Shadow-yellow"></i> </label>
              <p class="color-title">Light Yellow</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color04" name="color" data-color="light-blue.jpg" data-colorName="Light Blue" data-priceTag="0">
              <label title="light blue" for="color04"> <i class="Shadow-blue"></i> </label>
              <p class="color-title">Light Blue</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color09" name="color" data-color="dark-blue.jpg" data-colorName="Dark Blue" data-priceTag="0">
              <label title="navy blue" for="color09"> <i class="Shadow-darkblue"></i> </label>
              <p class="color-title">Dark Blue</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color02" name="color" data-color="dark-green.jpg" data-colorName="Dark Green" data-priceTag="0">
              <label title="dark green" for="color02"> <i class="Shadow-green"></i> </label>
              <p class="color-title">Dark Green</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color055" name="color" data-color="light-green.jpg" data-colorName="Light Green" data-priceTag="0">
              <label title="Bright green" for="color055"> <i class="Shadow-light-green"></i> </label>
              <p class="color-title">Light Green</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color066" name="color" data-color="moroon.jpg" data-colorName="Maroon" data-priceTag="450">
              <label title="moroon" for="color066"> <i class="Shadow-moroon"></i> </label>
              <p class="color-title">Maroon<br>
                <small class="price-tag"><!--Add $ 450--></small></p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color10" name="color" data-color="orange.jpg" data-colorName="Orange" data-priceTag="595">
              <label title="Orange" for="color10"> <i class="Shadow-orange"></i> </label>
              <p class="color-title">Orange<br>
                <small class="price-tag"><!--Add $ 595--></small></p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color05" name="color" data-color="dark-grey.jpg" data-colorName="Dark Grey" data-priceTag="395">
              <label title="dark grey" for="color05"> <i class="Shadow-grey"></i> </label>
              <p class="color-title">Dark Grey<br>
                <small class="price-tag"><!--Add $ 395--></small></p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color099" name="color" data-color="cream.jpg" data-colorName="Smooth Cream" data-priceTag="470">
              <label title="Cream" for="color099"> <i class="Shadow-cream"></i> </label>
              <p class="color-title">Smooth Cream<br>
                <small class="price-tag"><!--Add $ 470--></small></p>
            </div>
          </div>
           
          <p class="colorNote">We can paint the vehicle in any color</p>
           
           
           
          <div class="bodyColor2" id="tool_rim">
            <h3 class="heading-color">Wheel color</h3>
            <div class="colorBox">
              <input type="radio" id="color101" name="color-2" checked="checked" data-color="rim-silver.png" data-wheelsColor="Chrome">
              <label title="Silver" for="color101"> <i class="Shadow-silver"></i> </label>
              <p class="color-title">Chrome</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color102" name="color-2" data-color="rim-dark-grey.png" data-wheelsColor="Dark Grey">
              <label title="Dark-grey" for="color102"> <i class="Shadow-grey"></i> </label>
              <p class="color-title">Dark Grey</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color103" name="color-2" data-color="rim-black.png" data-wheelsColor="Shadow Black">
              <label title="Black" for="color103"> <i class="Shadow-Black"></i> </label>
              <p class="color-title">Shadow Black</p>
            </div> 
          </div>
           
            
        </div>
      </div>
    </div>
  </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
            <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="panel-title expand">
            <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
            <a href="javascript:void(0);"><i class="fa fa-car-battery"></i>Powertrain</a> </h4>
        </div>
        <div id="collapse2" class="panel-collapse collapse">
            <div class="panel-body">
                <h3 class="contant-hed">Engine/Transmissions</h3>
                <div class="option-view"> 
                    <img src="/wp-content/themes/Avada/images/options/430HP-Man.jpg"> 
                </div> 
                                                            
        <div class="dataRow">
            <h3 class="heading-options">Modern Performance</h3>
            <div class="optBox">
                <label title="430 Horsepower Manual" for="430HP-Man" class="label_container">430 Horsepower LS3 Engine with 6 Speed Manual Transmission <strong>$42,678.00</strong> 
                <input type="radio" id="430HP-Man" checked="checked" name="Engine-and-Trans" class="custom_radio" data-elementName="430 Horsepower LS3 Engine with 6 Speed Manual Transmission" data-pricetag="42678">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label title="430 Horsepower Automatic" for="430HP-Auto" class="label_container">430 Horsepower LS3 Engine with 4L65E Automatic Transmission <strong>$42,972.30</strong> 
                <input type="radio" id="430HP-Auto" name="Engine-and-Trans" class="custom_radio" data-pricetag="42972.30" data-elementName="430 Horsepower LS3 Engine with 4L65E Automatic Transmission">
                <span class="checkmark"></span> </label>
            </div>
        </div>
        
        <div class="dataRow">
            <h3 class="heading-options">High Performance </h3>
            <div class="optBox">
                <label title="556 Horsepower Manual" for="556HP-Man" class="label_container">556 Horsepower LSA Supercharged Engine with 6 Speed Manual Transmission <strong>$50,471.55</strong> 
                <input type="radio" id="556HP-Man" name="Engine-and-Trans" class="custom_radio" data-pricetag="50471.55" data-elementName="556 Horsepower LSA Supercharged Engine with 6 Speed Manual Transmission">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label title="556 Horsepower Automatic" for="556HP-Auto" class="label_container">556 Horsepower LSA Supercharged Engine with 4L75E Automatic Transmission <strong>$50,876.55</strong> 
                <input type="radio" id="556HP-Auto" name="Engine-and-Trans" class="custom_radio" data-pricetag="50876.55" data-elementName="556 Horsepower LSA Supercharged Engine with 4L75E Automatic Transmission">
                <span class="checkmark"></span> </label>
            </div>
        </div>
        
        <div class="dataRow">
            <h3 class="heading-options">Competition Power</h3>
            <div class="optBox">
                <label title="815 Horsepower Automatic" for="815HP-Auto" class="label_container">815 Horsepower Edelbrock LTI Supercharged Engine with 8L90E Automatic Transmission <strong>$63,392.40</strong> 
                <input type="radio" id="815HP-Auto" name="Engine-and-Trans" class="custom_radio" data-pricetag="63392.40" data-elementName="815 Horsepower Edelbrock LTI Supercharged Engine with 8L90E Automatic Transmission">
                <span class="checkmark"></span> </label>
            </div> 
        </div> 
                                                                    
                                                                    
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fas fa-car-crash"></i>Brakes</a> </h4>
                                                    </div>
                                                    <div id="collapse3" class="panel-collapse collapse">
                                                        <div class="panel-body"> 
                                                        <div class="option-view">
															<img src="/wp-content/themes/Avada/images/options/DB-4piston.jpg"> 
                                                        </div> 
                                                        <h3 class="heading-options">Select Brake type</h3>
		<div class="dataRow"> 
            <div class="optBox">
                <label title="Disc Brake 4 Piston" for="DB-4piston" class="label_container">Front & Rear Power Disc Brake System with four wheel Wilwood 4 Piston Calipers and Drilled & Slotted Rotors <small>$</small><strong class="DB-Price"></strong> 
                <input type="radio" id="DB-4piston" checked="checked" name="brakes-type" class="custom_radio" data-elementName="Front & Rear Power Disc Brake System with four wheel Wilwood 4 Piston Calipers and Drilled & Slotted Rotors">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label title="Disc Brake 6 Piston" for="DB-6piston" class="label_container">Front & Rear Power Disc Brake System with Wilwood 6 Piston Front/4 Piston Rear Calipers and Drilled & Slotted Rotors <small>$</small><strong class="DB-Price"></strong>
                <input type="radio" id="DB-6piston" name="brakes-type" class="custom_radio" data-elementName="Front & Rear Power Disc Brake System with Wilwood 6 Piston Front/4 Piston Rear Calipers and Drilled & Slotted Rotors">
                <span class="checkmark"></span> </label>
            </div>
        </div>
        
 
                                                                    
                                                                    
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fas fa-couch"></i>Interior</a> </h4>
                                                    </div>
                                                    <div id="collapse4" class="panel-collapse collapse">
                                                        <div class="panel-body"> 
                                                            <div class="option-view">
                                                                <img src="/wp-content/themes/Avada/images/options/Stock-Original-inter.jpg"> 
                                                            </div>
                                                            <h3 class="heading-options">Select Interior type</h3>
		<div class="dataRow"> 
            <div class="optBox">
                <label for="Stock-Original-inter" class="label_container">Stock Original Interior <strong>$11,475.00</strong>
                <input type="radio" id="Stock-Original-inter" checked="checked" name="interior-type" class="custom_radio" data-elementName="Stock Original Interior" data-pricetag="11475">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Performance-Style-c-inter" class="label_container">Performance Style Custom Interior <strong>$20,250.00</strong>
                <input type="radio" id="Performance-Style-c-inter" name="interior-type" class="custom_radio" data-elementName="Performance Style Custom Interior" data-pricetag="20250">
                <span class="checkmark"></span> </label>
            </div> 
            <div class="optBox">
                <label for="Custom-Luxury-inter" class="label_container">Custom Luxury Interior - <strong>Call for pricing</strong> on Custom Luxury Interiors
                <input type="radio" id="Custom-Luxury-inter" name="interior-type" class="custom_radio" data-elementName="Custom Luxury Interior" data-pricetag="Call for pricing">
                <span class="checkmark"></span> </label>
            </div>
        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                   
                                                
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse6B" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus"></i>Extras</a> </h4>
                                                    </div>
                                                    <div id="collapse6B" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="option-view--">
                                                                <img src="/wp-content/themes/Avada/images/options/extra-options.jpg" style="max-width:100%"> 
                                                            </div>
                                                            <h3 class="heading-options">Select Extra Options</h3>
		<div class="dataRow"> 
            <div class="optBox">
                <label for="Climate-Control-ex" class="label_container">Climate Control
                <input type="checkbox" id="Climate-Control-ex" name="extra-options" class="custom_checkbox" data-optionName="Climate Control">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Windows-ex" class="label_container">Power Windows
                <input type="checkbox" id="Power-Windows-ex" name="extra-options" class="custom_checkbox" data-optionName="Power Windows">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Locks-ex" class="label_container">Keyless Entry Power Locks
                <input type="checkbox" id="Power-Locks-ex" name="extra-options" class="custom_checkbox" data-optionName="Keyless Entry Power Locks">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Locks-ss-ex" class="label_container">Keyless Entry Power Locks with Security System
                <input type="checkbox" id="Power-Locks-ss-ex" name="extra-options" class="custom_checkbox" data-optionName="Keyless Entry Power Locks with Security System">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Auto-Trunk-ex" class="label_container">Automatic Trunk Opening System
                <input type="checkbox" id="Auto-Trunk-ex" name="extra-options" class="custom_checkbox" data-optionName="Automatic Trunk Opening System">
                <span class="checkmark"></span> </label>
            </div> 
            <div class="optBox">
                <label for="Digital-Dash-ex" class="label_container">Digital Dash
                <input type="checkbox" id="Digital-Dash-ex" name="extra-options" class="custom_checkbox" data-optionName="Digital Dash">
                <span class="checkmark"></span> </label>
            </div>  
            <div class="optBox">
                <label for="Paddle-Shift-ex" class="label_container">Paddle Shift
                <input type="checkbox" id="Paddle-Shift-ex" name="extra-options" class="custom_checkbox" data-optionName="Paddle Shift">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Heated-Seats-ex" class="label_container">Heated Seats
                <input type="checkbox" id="Heated-Seats-ex" name="extra-options" class="custom_checkbox" data-optionName="Heated Seats">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Seats-ex" class="label_container">Power Seats
                <input type="checkbox" id="Power-Seats-ex" name="extra-options" class="custom_checkbox" data-optionName="Power Seats">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Media-Nav-System-ex" class="label_container">Media & Navigation System
                <input type="checkbox" id="Media-Nav-System-ex" name="extra-options" class="custom_checkbox" data-optionName="Media & Navigation System">
                <span class="checkmark"></span> </label>
            </div> 
            <div class="optBox">
                <label for="Mod-Sound-System-ex" class="label_container">Modern Sound System
                <input type="checkbox" id="Mod-Sound-System-ex" name="extra-options" class="custom_checkbox" data-optionName="Modern Sound System">
                <span class="checkmark"></span> </label>
            </div> 
            
        </div>                              
                                                        </div>                                  
                                                    </div>
                                                </div>
                                                
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse7B" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fa fa-car"></i>Packages</a> </h4>
                                                    </div>
                                                    <div id="collapse7B" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <h3 class="contant-hed">Ask us about our Spy Package.</h3>
                                                            <!--div class="power-train-data"> 
                                                                <img src="/wp-content/themes/Avada/images/1-1.jpg">
                                                                <ul>
                                                                    <li><i class="fa fa-angle-right"></i>Rear View Camera</li>
                                                                    <li><i class="fa fa-angle-right"></i>TrackApps™</li>
                                                                    <li><i class="fa fa-angle-right"></i>LED Headlamps with LED Signature Lighting</li>
                                                                    <li><i class="fa fa-angle-right"></i>Intelligent Access with Push Button Start</li>
                                                                    <li><i class="fa fa-angle-right"></i>Dual Bright Exhaust with Rolled Tips </li>
                                                                </ul>
                                                                <span>Included</span> 
                                                            </div-->
                                                        </div>
                                                    </div>
                                                </div>