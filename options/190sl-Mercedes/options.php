<div class="panel panel-default">
  <div class="panel-heading open">
    <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="panel-title expand">
      <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
      <a href="javascript:void(0);"><i class="fa fa-paint-brush"></i>Paint & Exterior</a> </h4>
  </div>
  <div id="collapse1" class="panel-collapse collapse in">
    <div class="tool-bar">
      <div class="tab-content">
        <div class="tab-pane active" id="body-color" data-folder="190sl-Mercedes">
          <!--<div class="bodyColor8" id="tool_color_type">
            <h3 class="heading-color">Choose Color Type </h3>
            <div class="colorBox">
              <label title="stock Color" for="stockColor" class="label_container">Matte Paint
                <input type="radio" id="stockColor" checked="" name="color-8" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>
            <div class="colorBox">
              <label title="Gloss Color" for="glossColor" class="label_container"> Gloss Paint
                <input type="radio" id="glossColor" name="color-8" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>
          </div>-->
          <!--<div class="bodyColor" id="tool_paint">
            <h3 class="heading-color">Matte Paint</h3>
             
            <div class="colorBox">
              <input type="radio" id="color00" name="color" checked="checked"  data-color="black.jpg" data-colorName="Shadow black" data-priceTag="0">
              <label title="black" for="color00"> <i class="Shadow-Black"></i> </label>
              <p class="color-title">Shadow Black</p>
            </div>
            
            <div class="colorBox">
              <input type="radio" id="color001" name="color" data-color="silver.jpg" data-colorName="Silver" data-priceTag="0">
              <label title="silver" for="color001"> <i class="Shadow-silver"></i> </label>
              <p class="color-title">Silver</p>
            </div>
            
            <div class="colorBox">
              <input type="radio" id="color002" name="color" data-color="light-silver.jpg" data-colorName="Light Silver" data-priceTag="0">
              <label title="silver" for="color002"> <i class="Light-silver"></i> </label>
              <p class="color-title">Light Silver</p>
            </div>
            
            <div class="colorBox">
              <input type="radio" id="color003" name="color" data-color="dark-silver.jpg" data-colorName="Dark Silver" data-priceTag="0">
              <label title="silver" for="color003"> <i class="Dark-silver"></i> </label>
              <p class="color-title">Dark Silver</p>
            </div> 
            
          </div>-->
          
          
          <input type="radio" id="glossColor" name="color-8" class="custom_radio" checked="checked" style="display:none">
          <div class="bodyColor6" id="tool_paint_gloss">
            <h3 class="heading-color">Gloss Paint </h3>
            <div class="colorBox">
              <input type="radio" id="color01" name="color" checked="checked"  data-color="black.jpg" data-colorName="Shadow black" data-priceTag="0">
              <label title="black" for="color01"> <i class="Shadow-Black"></i> </label>
              <p class="color-title">Shadow Black</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color06" name="color" data-color="red.jpg" data-colorName="Red" data-priceTag="0">
              <label title="red" for="color06"> <i class="Shadow-red"></i> </label>
              <p class="color-title">Red</p>
            </div>  
            <div class="colorBox">
              <input type="radio" id="colorgb" name="color" data-color="dark-blue.jpg" data-colorName="Dark Blue" data-priceTag="0">
              <label title="Dark Blue" for="colorgb"> <i class="Shadow-blue"></i> </label>
              <p class="color-title">Dark Blue</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color07" name="color" data-color="silver.jpg" data-colorName="Silver" data-priceTag="0">
              <label title="silver" for="color07"> <i class="Shadow-silver"></i> </label>
              <p class="color-title">Silver</p>
            </div> 
            
            <div class="colorBox">
              <input type="radio" id="color7C" name="color" data-color="squash.jpg" data-colorName="Squash" data-priceTag="0">
              <label title="squash" for="color7C"> <i class="Shadow-squash"></i> </label>
              <p class="color-title">Squash</p>
            </div> 
            <div class="colorBox">
              <input type="radio" id="color03" name="color" data-color="white.jpg" data-colorName="White" data-priceTag="0">
              <label title="white" for="color03"> <i class="Shadow-white"></i> </label>
              <p class="color-title">White</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color04" name="color" data-color="grey-blue.jpg" data-colorName="Grey Blue" data-priceTag="0">
              <label title="grey blue" for="color04"> <i class="Shadow-grey-blue"></i> </label>
              <p class="color-title">Grey blue</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color055" name="color" data-color="sky-blue.jpg" data-colorName="Sky Blue" data-priceTag="0">
              <label title="Bright green" for="color055"> <i class="Shadow-sky-blue"></i> </label>
              <p class="color-title">Sky Blue</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color066" name="color" data-color="cream.jpg" data-colorname="Cream" data-pricetag="0">
              <label title="Cream" for="color066"> <i class="Shadow-cream"></i> </label>
              <p class="color-title">Cream<br> 
            </div>
            <div class="colorBox">
              <input type="radio" id="colorTeal" name="color" data-color="teal.jpg" data-colorname="Teal" data-pricetag="0">
              <label title="Teal" for="colorTeal"> <i class="Shadow-teal"></i> </label>
              <p class="color-title">Teal<br> 
            </div>
            <div class="colorBox">
              <input type="radio" id="colorBronze" name="color" data-color="bronze.jpg" data-colorname="Bronze" data-pricetag="0">
              <label title="Bronze" for="colorBronze"> <i class="Shadow-bronze"></i> </label>
              <p class="color-title">Bronze<br> 
            </div>
          </div>
          
          <p class="colorNote">We can paint the vehicle in any color</p>
          
          
          
          
           
          <div class="bodyColor4" id="tool_tire">
            <h3 class="heading-color">Customize Wheels</h3>
            <div class="colorBox">
              <label title="Stock Tire" for="stock" class="label_container">Original Wheels
                <input type="radio" id="stock" checked="checked" name="color-4" data-color="transparent.png" data-wheelsName="Original Wheels" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>
            
            <div class="colorBox">
              <label title="Modern Chrome Wheels" for="modern" class="label_container">Modern Chrome Wheels
                <input type="radio" id="modern" name="color-4" data-color="rim-mod-chrome.png" data-wheelsName="Modern Chrome Wheels" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>
            
            
            <div class="colorBox" id="checkBlack">
              <label title="Modern black Wheels" for="mod-black" class="label_container">Modern black Wheels
                <input type="radio" id="mod-black" name="color-4" data-color="mod-black-tire.png" data-wheelsName="Modern black Wheels" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>
            <!--<div class="colorBox">
              <label title="Modern Tire" for="modern" class="label_container">Modern Chrome Wheels
                <input type="radio" id="modern" name="color-4" data-color="rim-mod-chrome.png" data-wheelsName="Modern Chrome Wheels" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>-->
          </div>
          
          <div class="bodyColor2" id="tool_rim">
            <h3 class="heading-color">Wheel color</h3>
            <div class="colorBox">
              <input type="radio" id="color101" name="color-2" checked="checked" data-color="rim-chrome.png" data-wheelsColor="Chrome">
              <label title="Chrome" for="color101"> <i class="Shadow-silver"></i> </label>
              <p class="color-title">Chrome</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color102" name="color-2" data-color="rim-dark-grey.png" data-wheelsColor="Dark Grey">
              <label title="Dark-grey" for="color102"> <i class="Shadow-grey"></i> </label>
              <p class="color-title">Dark Grey</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color103" name="color-2" data-color="rim-black.png" data-wheelsColor="Shadow Black">
              <label title="Black" for="color103"> <i class="Shadow-Black"></i> </label>
              <p class="color-title">Shadow Black</p>
            </div> 
          </div>
          
          <!--<div class="bodyColor2" id="tool_mod_rim">
            <h3 class="heading-color">Modern Wheel color</h3>
            <div class="colorBox">
              <input type="radio" id="modRim-chrome" name="modRim-color" checked="checked" data-color="rim-mod-chrome.png" data-rimsColor="Chrome">
              <label title="Chrome" for="modRim-chrome"> <i class="Shadow-silver"></i> </label>
              <p class="color-title">Chrome</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="modRim-black" name="modRim-color" data-color="rim-mod-black.png" data-rimsColor="Shadow Black">
              <label title="Shadow Black" for="modRim-black"> <i class="Shadow-Black"></i> </label>
              <p class="color-title">Shadow Black</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="modRim-red" name="modRim-color" data-color="rim-mod-red.png" data-rimsColor="Red">
              <label title="Red" for="modRim-red"> <i class="Shadow-red"></i> </label>
              <p class="color-title">Red</p>
            </div> 
            <div class="colorBox">
              <input type="radio" id="modRim-lightBlue" name="modRim-color" data-color="rim-mod-light-blue.png" data-rimsColor="Light Blue">
              <label title="Red" for="modRim-lightBlue"> <i class="Shadow-blue"></i> </label>
              <p class="color-title">Blue</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="modRim-green" name="modRim-color" data-color="rim-mod-green.png" data-rimsColor="Light Green">
              <label title="Green" for="modRim-green"> <i class="Shadow-light-green"></i> </label>
              <p class="color-title">Green</p>
            </div> 
          </div>-->
           
          <div class="bodyColor2" id="tool_inter_color">
            <h3 class="heading-color">Interior Color</h3>
            <div class="colorBox">
              <input type="radio" id="interBlack" name="interior-color" checked="checked" data-color="interior-black.png" data-interColor="Black">
              <label title="Black" for="interBlack"><i class="Shadow-Black"></i></label>
              <p class="color-title">Black</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="interWhite" name="interior-color" data-color="interior-white.png" data-interColor="White">
              <label title="White" for="interWhite"><i class="Shadow-white"></i></label>
              <p class="color-title">White</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="interRed" name="interior-color" data-color="interior-red.png" data-interColor="Red">
              <label title="Red" for="interRed"><i class="Shadow-red"></i></label>
              <p class="color-title">Red</p>
            </div>
            <!--
             <div class="colorBox">
              <input type="radio" id="interTan" name="interior-color" data-color="interior-tan.png" data-interColor="Tan">
              <label title="Tan" for="interTan"><i class="Shadow-tan"></i></label>
              <p class="color-title">Tan</p>
            </div>
             <div class="colorBox">
              <input type="radio" id="interRB" name="interior-color" data-color="interior-rich-brown.png" data-interColor="Rich Brown">
              <label title="Rich Brown" for="interRB"><i class="Shadow-brown"></i></label>
              <p class="color-title">Rich Brown</p>
            </div>-->
            
          </div>
           
        </div>
      </div>
    </div>
  </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="panel-title expand">
            <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
            <a href="javascript:void(0);"><i class="fa fa-car-battery"></i>Powertrain</a> </h4>
        </div>
        <div id="collapse2" class="panel-collapse collapse">
            <div class="panel-body">
                <h3 class="contant-hed">Engine/Transmissions</h3>
                <div class="option-view">
                    <img src="/wp-content/themes/Avada//wp-content/themes/Avada/images/options/430HP-Man.jpg">
                </div>
                <div class="dataRow">
                    <h3 class="heading-options">Modern Performance</h3>
                    <div class="optBox">
                        <label title="430 Horsepower Manual" for="430HP-Man" class="label_container">430 Horsepower LS3 Engine with 6 Speed Manual Transmission
                            <strong>$42,678.00</strong>
                            <input type="radio" id="430HP-Man" checked="checked" name="Engine-and-Trans" class="custom_radio" data-elementName="430 Horsepower LS3 Engine with 6 Speed Manual Transmission" data-pricetag="42678">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label title="430 Horsepower Automatic" for="430HP-Auto" class="label_container">430 Horsepower LS3 Engine with 4L65E Automatic Transmission
                            <strong>$42,972.30</strong>
                            <input type="radio" id="430HP-Auto" name="Engine-and-Trans" class="custom_radio" data-pricetag="42972.30" data-elementName="430 Horsepower LS3 Engine with 4L65E Automatic Transmission">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
                <div class="dataRow">
                    <h3 class="heading-options">High Performance </h3>
                    <div class="optBox">
                        <label title="556 Horsepower Manual" for="556HP-Man" class="label_container">556 Horsepower LSA Supercharged Engine with 6 Speed Manual Transmission
                            <strong>$50,471.55</strong>
                            <input type="radio" id="556HP-Man" name="Engine-and-Trans" class="custom_radio" data-pricetag="50471.55" data-elementName="556 Horsepower LSA Supercharged Engine with 6 Speed Manual Transmission">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label title="556 Horsepower Automatic" for="556HP-Auto" class="label_container">556 Horsepower LSA Supercharged Engine with 4L75E Automatic Transmission
                            <strong>$50,876.55</strong>
                            <input type="radio" id="556HP-Auto" name="Engine-and-Trans" class="custom_radio" data-pricetag="50876.55" data-elementName="556 Horsepower LSA Supercharged Engine with 4L75E Automatic Transmission">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
                <div class="dataRow">
                    <h3 class="heading-options">Competition Power</h3>
                    <div class="optBox">
                        <label title="815 Horsepower Automatic" for="815HP-Auto" class="label_container">815 Horsepower Edelbrock LTI Supercharged Engine with 8L90E Automatic Transmission
                            <strong>$63,392.40</strong>
                            <input type="radio" id="815HP-Auto" name="Engine-and-Trans" class="custom_radio" data-pricetag="63392.40" data-elementName="815 Horsepower Edelbrock LTI Supercharged Engine with 8L90E Automatic Transmission">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="panel-title expand">
                <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                <a href="javascript:void(0);"><i class="fas fa-car-crash"></i>Brakes</a>
            </h4>
        </div>
        <div id="collapse3" class="panel-collapse collapse">
            <div class="panel-body"> 
                <div class="option-view">
                    <img src="/wp-content/themes/Avada//wp-content/themes/Avada/images/options/DB-4piston.jpg">
                </div>
                <h3 class="heading-options">Select Brake type</h3>
                <div class="dataRow">
                    <div class="optBox">
                        <label title="Disc Brake 4 Piston" for="DB-4piston" class="label_container">Front & Rear Power Disc Brake System with four wheel Wilwood 4 Piston Calipers and Drilled & Slotted Rotors
                            <small>$</small><strong class="DB-Price"></strong>
                            <input type="radio" id="DB-4piston" checked="checked" name="brakes-type" class="custom_radio" data-elementName="Front & Rear Power Disc Brake System with four wheel Wilwood 4 Piston Calipers and Drilled & Slotted Rotors">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label title="Disc Brake 6 Piston" for="DB-6piston" class="label_container">Front & Rear Power Disc Brake System with Wilwood 6 Piston Front/4 Piston Rear Calipers and Drilled & Slotted Rotors
                            <small>$</small><strong class="DB-Price"></strong>
                            <input type="radio" id="DB-6piston" name="brakes-type" class="custom_radio" data-elementName="Front & Rear Power Disc Brake System with Wilwood 6 Piston Front/4 Piston Rear Calipers and Drilled & Slotted Rotors">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="panel-title expand">
                <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                <a href="javascript:void(0);"><i class="fas fa-couch"></i>Interior</a>
            </h4>
        </div>
        <div id="collapse4" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="option-view">
                    <img src="/wp-content/themes/Avada//wp-content/themes/Avada/images/options/Stock-Original-inter.jpg">
                </div>
                <h3 class="heading-options">Select Interior type</h3>
                <div class="dataRow">
                    <div class="optBox">
                        <label for="Stock-Original-inter" class="label_container">Stock Original Interior <strong>$11,475.00</strong>
                            <input type="radio" id="Stock-Original-inter" checked="checked" name="interior-type" class="custom_radio" data-elementName="Stock Original Interior" data-pricetag="11475">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label for="Performance-Style-c-inter" class="label_container">Performance Style Custom Interior
                            <strong>$20,250.00</strong>
                            <input type="radio" id="Performance-Style-c-inter" name="interior-type" class="custom_radio" data-elementName="Performance Style Custom Interior" data-pricetag="20250">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label for="Custom-Luxury-inter" class="label_container">Custom Luxury Interior -
                            <strong>Call for pricing</strong>
                            on Custom Luxury Interiors
                            <input type="radio" id="Custom-Luxury-inter" name="interior-type" class="custom_radio" data-elementName="Custom Luxury Interior" data-pricetag="Call for pricing">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    
                     
                     
                    
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse6B" class="panel-title expand">
                <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                <a href="javascript:void(0);"><i class="fa fa-cart-plus"></i>Extras</a>
            </h4>
        </div>
        <div id="collapse6B" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="option-view--">
                    <img src="/wp-content/themes/Avada/images/options/extra-options.jpg" style="max-width:100%">
                </div>
                <h3 class="heading-options">Select Extra Options</h3>
                <div class="dataRow">
                    <div class="optBox">
                        <label for="Climate-Control-ex" class="label_container">Climate Control
                            <input type="checkbox" id="Climate-Control-ex" name="extra-options" class="custom_checkbox" data-optionName="Climate Control">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label for="Power-Windows-ex" class="label_container">Power Windows
                            <input type="checkbox" id="Power-Windows-ex" name="extra-options" class="custom_checkbox" data-optionName="Power Windows">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label for="Power-Locks-ex" class="label_container">Keyless Entry Power Locks
                            <input type="checkbox" id="Power-Locks-ex" name="extra-options" class="custom_checkbox" data-optionName="Keyless Entry Power Locks">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label for="Power-Locks-ss-ex" class="label_container">Keyless Entry Power Locks with Security System
                            <input type="checkbox" id="Power-Locks-ss-ex" name="extra-options" class="custom_checkbox" data-optionName="Keyless Entry Power Locks with Security System">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label for="Auto-Trunk-ex" class="label_container">Automatic Trunk Opening System
                            <input type="checkbox" id="Auto-Trunk-ex" name="extra-options" class="custom_checkbox" data-optionName="Automatic Trunk Opening System">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label for="Digital-Dash-ex" class="label_container">Digital Dash
                            <input type="checkbox" id="Digital-Dash-ex" name="extra-options" class="custom_checkbox" data-optionName="Digital Dash">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label for="Paddle-Shift-ex" class="label_container">Paddle Shift
                            <input type="checkbox" id="Paddle-Shift-ex" name="extra-options" class="custom_checkbox" data-optionName="Paddle Shift">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label for="Heated-Seats-ex" class="label_container">Heated Seats
                            <input type="checkbox" id="Heated-Seats-ex" name="extra-options" class="custom_checkbox" data-optionName="Heated Seats">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label for="Power-Seats-ex" class="label_container">Power Seats
                            <input type="checkbox" id="Power-Seats-ex" name="extra-options" class="custom_checkbox" data-optionName="Power Seats">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label for="Media-Nav-System-ex" class="label_container">Media & Navigation System
                            <input type="checkbox" id="Media-Nav-System-ex" name="extra-options" class="custom_checkbox" data-optionName="Media & Navigation System">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="optBox">
                        <label for="Mod-Sound-System-ex" class="label_container">Modern Sound System
                            <input type="checkbox" id="Mod-Sound-System-ex" name="extra-options" class="custom_checkbox" data-optionName="Modern Sound System">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse7B" class="panel-title expand">
                <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                <a href="javascript:void(0);"><i class="fa fa-car"></i>Packages</a>
            </h4>
        </div>
        <div id="collapse7B" class="panel-collapse collapse">
            <div class="panel-body">
                <h3 class="contant-hed">Ask us about our Spy Package.</h3>
                <!--div class="power-train-data">
                    <img src="/wp-content/themes/Avada/images/1-1.jpg">
                    <ul>
                        <li><i class="fa fa-angle-right"></i>Rear View Camera</li>
                        <li><i class="fa fa-angle-right"></i>TrackApps™</li>
                        <li><i class="fa fa-angle-right"></i>LED Headlamps with LED Signature Lighting</li>
                        <li><i class="fa fa-angle-right"></i>Intelligent Access with Push Button Start</li>
                        <li><i class="fa fa-angle-right"></i>Dual Bright Exhaust with Rolled Tips </li>
                    </ul>
                    <span>Included</span>
                </div-->
            </div>
        </div>
    </div>