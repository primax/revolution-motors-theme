<div class="panel panel-default">
  <div class="panel-heading open">
    <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="panel-title expand">
      <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
      <a href="javascript:void(0);"><i class="fa fa-paint-brush"></i>Paint & Exterior</a> </h4>
  </div>
  <div id="collapse1" class="panel-collapse collapse in">
    <div class="tool-bar">
      <div class="tab-content">
        <div class="tab-pane active" id="body-color" data-folder="jeep">
           
          <div class="bodyColor" id="tool_paint">
            <h3 class="heading-color">Matte Paint</h3>
            <div class="colorBox">
              <input type="radio" id="color01" name="color" checked="checked"  data-color="black.jpg" data-colorName="Shadow black" data-priceTag="0">
              <label title="black" for="color01"> <i class="Shadow-Black"></i> </label>
              <p class="color-title">Shadow Black</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color03" name="color" data-color="white.jpg" data-colorName="Oxford white" data-priceTag="0">
              <label title="white" for="color03"> <i class="Shadow-white"></i> </label>
              <p class="color-title">Oxford white</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color07" name="color" data-color="silver.jpg" data-colorName="Ingot Silver" data-priceTag="0">
              <label title="silver" for="color07"> <i class="Shadow-silver"></i> </label>
              <p class="color-title">Ingot Silver</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color06" name="color" data-color="red.jpg" data-colorName="Red" data-priceTag="0">
              <label title="red" for="color06"> <i class="Shadow-red"></i> </label>
              <p class="color-title"> Red</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color08" name="color" data-color="yellow.jpg" data-colorName="Light Yellow" data-priceTag="0">
              <label title="yellow" for="color08"> <i class="Shadow-yellow"></i> </label>
              <p class="color-title">Light Yellow</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color04" name="color" data-color="light-blue.jpg" data-colorName="Light Blue" data-priceTag="0">
              <label title="light blue" for="color04"> <i class="Shadow-blue"></i> </label>
              <p class="color-title">Light Blue</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color09" name="color" data-color="dark-blue.jpg" data-colorName="Dark Blue" data-priceTag="0">
              <label title="navy blue" for="color09"> <i class="Shadow-darkblue"></i> </label>
              <p class="color-title">Dark Blue</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color02" name="color" data-color="dark-green.jpg" data-colorName="Dark Green" data-priceTag="0">
              <label title="dark green" for="color02"> <i class="Shadow-green"></i> </label>
              <p class="color-title">Dark Green</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color055" name="color" data-color="light-green.jpg" data-colorName="Light Green" data-priceTag="0">
              <label title="Bright green" for="color055"> <i class="Shadow-light-green"></i> </label>
              <p class="color-title">Light Green</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color066" name="color" data-color="moroon.jpg" data-colorName="Maroon" data-priceTag="450">
              <label title="moroon" for="color066"> <i class="Shadow-moroon"></i> </label>
              <p class="color-title">Maroon<br>
                <small class="price-tag"><!--Add $ 450--></small></p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color10" name="color" data-color="orange.jpg" data-colorName="Orange" data-priceTag="595">
              <label title="Orange" for="color10"> <i class="Shadow-orange"></i> </label>
              <p class="color-title">Orange<br>
                <small class="price-tag"><!--Add $ 595--></small></p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color05" name="color" data-color="dark-grey.jpg" data-colorName="Dark Grey" data-priceTag="395">
              <label title="dark grey" for="color05"> <i class="Shadow-grey"></i> </label>
              <p class="color-title">Dark Grey<br>
                <small class="price-tag"><!--Add $ 395--></small></p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color099" name="color" data-color="cream.jpg" data-colorName="Smooth Cream" data-priceTag="470">
              <label title="Cream" for="color099"> <i class="Shadow-cream"></i> </label>
              <p class="color-title">Smooth Cream<br>
                <small class="price-tag"><!--Add $ 470--></small></p>
            </div>
          </div>
           
          <p class="colorNote">We can paint the vehicle in any color</p>
           
          <div class="bodyColor3" id="tool_roof">
            <h3 class="heading-color">Roof Color</h3>
            <div class="colorBox">
              <input type="radio" id="color100" name="color-3" checked=""  data-color="roof-black.png">
              <label title="black" for="color100"> <i class="Shadow-Black"></i> </label>
              <p class="color-title">Shadow Black</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color999" name="color-3"  data-color="roof-dark-green.png">
              <label title="Dark-green" for="color999"> <i class="Shadow-green"></i> </label>
              <p class="color-title">Light Green</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color998" name="color-3"  data-color="roof-dark-grey.png">
              <label title="Dark-grey" for="color998"> <i class="Shadow-grey"></i> </label>
              <p class="color-title">Dark Grey</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color997" name="color-3"  data-color="roof-moroon.png">
              <label title="Moroon" for="color997"> <i class="Shadow-moroon"></i> </label>
              <p class="color-title">Maroon</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color996" name="color-3"  data-color="roof-cream.png">
              <label title="Cream" for="color996"> <i class="Shadow-cream"></i> </label>
              <p class="color-title">Cream</p>
            </div>
          </div>
           
          
          
          <div class="bodyColor2" id="tool_rim">
            <h3 class="heading-color">Wheel color</h3>
            <div class="colorBox">
              <input type="radio" id="color101" name="color-2" checked="checked" data-color="rim-silver.png" data-wheelsColor="Chrome">
              <label title="Silver" for="color101"> <i class="Shadow-silver"></i> </label>
              <p class="color-title">Chrome</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color102" name="color-2" data-color="rim-dark-grey.png" data-wheelsColor="Dark Grey">
              <label title="Dark-grey" for="color102"> <i class="Shadow-grey"></i> </label>
              <p class="color-title">Dark Grey</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color103" name="color-2" data-color="rim-black.png" data-wheelsColor="Shadow Black">
              <label title="Black" for="color103"> <i class="Shadow-Black"></i> </label>
              <p class="color-title">Shadow Black</p>
            </div>
            <!--<div class="colorBox">
              <input type="radio" id="color104" name="color-2"  data-color="rim-moroon.png" data-wheelsColor="Maroon">
              <label title="moroon" for="color104"> <i class="Shadow-moroon"></i> </label>
              <p class="color-title">Maroon</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color105" name="color-2" data-color="rim-dark-blue.png" data-wheelsColor="Dark Blue">
              <label title="dark-blue" for="color105"> <i class="Shadow-darkblue"></i> </label>
              <p class="color-title">Dark Blue</p>
            </div>-->
          </div>
           
           
           
           
        </div>
      </div>
    </div>
  </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="panel-title expand">
        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
        <a href="javascript:void(0);"><i class="fa fa-car-battery"></i>Powertrain</a> </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
            <h3 class="contant-hed">Engine/Transmissions</h3>
            <div class="option-view"> 
                <img src="/wp-content/themes/Avada/images/options/430HP-Man.jpg"> 
            </div> 
                                                            
        <div class="dataRow">  
        <br />
            <div class="optBox">
                <label title="425 Horsepower Auto" for="425HP-Auto" class="label_container">425 Horsepower Coyote Engine with 4R7OW 4 Speed Automatic Transmission <strong></strong> 
                <input type="radio" id="425HP-Auto" checked="checked" name="Engine-and-Trans" class="custom_radio" data-elementName="425 Horsepower Coyote Engine with 4R7OW 4 Speed Automatic Transmission" data-pricetag="">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label title="435 Horsepower Auto" for="435HP-Man" class="label_container">435 Horsepower Coyote Engine with 5 Speed Overdrive Transmission <strong></strong> 
                <input type="radio" id="435HP-Man" checked="checked" name="Engine-and-Trans" class="custom_radio" data-elementName="435 Horsepower Coyote Engine with 5 Speed Overdrive Transmission" data-pricetag="">
                <span class="checkmark"></span> </label>
            </div>
            
            <div class="optBox">
                <label title="630 Horsepower Auto" for="630HP-Auto" class="label_container">630 Horsepower Coyote Engine with Vortech Supercharger and 6R80 6 Speed Automatic Transmission <strong></strong> 
                <input type="radio" id="630HP-Auto" checked="checked" name="Engine-and-Trans" class="custom_radio" data-elementName="630 Horsepower Coyote Engine with Vortech Supercharger and 6R80 6 Speed Automatic Transmission" data-pricetag="">
                <span class="checkmark"></span> </label>
            </div>
            
            <div class="optBox">
                <label title="727 Horsepower Auto" for="727HP-Auto" class="label_container">727 Horsepower Coyote Engine with Roush Supercharger and 6R80 6 Speed Automatic Transmission <strong></strong> 
                <input type="radio" id="727HP-Auto" checked="checked" name="Engine-and-Trans" class="custom_radio" data-elementName="727 Horsepower Coyote Engine with Roush Supercharger and 6R80 6 Speed Automatic Transmission" data-pricetag="">
                <span class="checkmark"></span> </label>
            </div>
            
            <div class="optBox">
                <label title="785 Horsepower Auto" for="785HP-Auto" class="label_container">785 Horsepower Coyote Engine with Edelbrock Supercharger and 6R80 6 Speed Automatic Transmission <strong></strong> 
                <input type="radio" id="785HP-Auto" checked="checked" name="Engine-and-Trans" class="custom_radio" data-elementName="785 Horsepower Coyote Engine with Edelbrock Supercharger and 6R80 6 Speed Automatic Transmission" data-pricetag="">
                <span class="checkmark"></span> </label>
            </div>
            
            



            
        </div> 
                                                                    
                                                                    
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fa fa-sliders"></i>Chassis & Suspension</a> </h4>
                                                    </div>
                                                    <div id="collapse3" class="panel-collapse collapse">
                                                        <div class="panel-body"> 
                                                            <div class="option-view"> 
                                                                <img src="/wp-content/themes/Avada/images/chassis-and-suspension.jpg"> 
                                                            </div> 
                           <br>                                 
        <div class="dataRow">
        <label>High Performance Custom Chassis with Quad Link Suspension</label>
        </div> 
                                                                    
                                                                    
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fas fa-car-crash"></i>Brakes</a> </h4>
                                                    </div>
                                                    <div id="collapse4" class="panel-collapse collapse">
                                                        <div class="panel-body"> 
                                                        <div class="option-view">
															<img src="/wp-content/themes/Avada/images/options/DB-4piston.jpg"> 
                                                        </div> 
                                                        <h3 class="heading-options">Select Brake type</h3>
		<div class="dataRow"> 
            <div class="optBox">
                <label title="Disc Brake 4 Piston" for="DB-4piston" class="label_container">Front & Rear Power Disc Brake System with four wheel Wilwood 4 Piston Calipers and Drilled & Slotted Rotors <small>$</small><strong class="DB-Price"></strong> 
                <input type="radio" id="DB-4piston" checked="checked" name="brakes-type" class="custom_radio" data-elementName="Front & Rear Power Disc Brake System with four wheel Wilwood 4 Piston Calipers and Drilled & Slotted Rotors">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label title="Disc Brake 6 Piston" for="DB-6piston" class="label_container">Front & Rear Power Disc Brake System with Wilwood 6 Piston Front/4 Piston Rear Calipers and Drilled & Slotted Rotors <small>$</small><strong class="DB-Price"></strong>
                <input type="radio" id="DB-6piston" name="brakes-type" class="custom_radio" data-elementName="Front & Rear Power Disc Brake System with Wilwood 6 Piston Front/4 Piston Rear Calipers and Drilled & Slotted Rotors">
                <span class="checkmark"></span> </label>
            </div>
        </div>
        
 
                                                                    
                                                                    
                                                        </div>
                                                    </div>
                                                </div>
                                                  
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fas fa-couch"></i>Interior</a> </h4>
                                                    </div>
                                                    <div id="collapse5" class="panel-collapse collapse">
                                                        <div class="panel-body"> 
                                                            <div class="option-view">
                                                                <img src="/wp-content/themes/Avada/images/options/Stock-Original-inter.jpg"> 
                                                            </div>
                                                            <h3 class="heading-options">Select Interior type</h3>
		<div class="dataRow"> 
            <div class="optBox">
                <label for="Stock-Original-inter" class="label_container">Stock Original Interior <strong>$11,475.00</strong>
                <input type="radio" id="Stock-Original-inter" checked="checked" name="interior-type" class="custom_radio" data-elementName="Stock Original Interior" data-pricetag="11475">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Performance-Style-c-inter" class="label_container">Performance Style Custom Interior <strong>$20,250.00</strong>
                <input type="radio" id="Performance-Style-c-inter" name="interior-type" class="custom_radio" data-elementName="Performance Style Custom Interior" data-pricetag="20250">
                <span class="checkmark"></span> </label>
            </div> 
            <div class="optBox">
                <label for="Custom-Luxury-inter" class="label_container">Custom Luxury Interior - <strong>Call for pricing</strong> on Custom Luxury Interiors
                <input type="radio" id="Custom-Luxury-inter" name="interior-type" class="custom_radio" data-elementName="Custom Luxury Interior" data-pricetag="Call for pricing">
                <span class="checkmark"></span> </label>
            </div>
        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 
                                                
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse6" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus"></i>Extras</a> </h4>
                                                    </div>
                                                    <div id="collapse6" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="option-view--">
                                                                <img src="/wp-content/themes/Avada/images/options/extra-options.jpg" style="max-width:100%"> 
                                                            </div>
                                                            <h3 class="heading-options">Select Extra Options</h3>
		<div class="dataRow"> 
            <div class="optBox">
                <label for="Climate-Control-ex" class="label_container">Climate Control
                <input type="checkbox" id="Climate-Control-ex" name="extra-options" class="custom_checkbox" data-optionName="Climate Control">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Windows-ex" class="label_container">Power Windows
                <input type="checkbox" id="Power-Windows-ex" name="extra-options" class="custom_checkbox" data-optionName="Power Windows">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Locks-ex" class="label_container">Keyless Entry Power Locks
                <input type="checkbox" id="Power-Locks-ex" name="extra-options" class="custom_checkbox" data-optionName="Keyless Entry Power Locks">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Locks-ss-ex" class="label_container">Keyless Entry Power Locks with Security System
                <input type="checkbox" id="Power-Locks-ss-ex" name="extra-options" class="custom_checkbox" data-optionName="Keyless Entry Power Locks with Security System">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Auto-Trunk-ex" class="label_container">Automatic Trunk Opening System
                <input type="checkbox" id="Auto-Trunk-ex" name="extra-options" class="custom_checkbox" data-optionName="Automatic Trunk Opening System">
                <span class="checkmark"></span> </label>
            </div> 
            <div class="optBox">
                <label for="Digital-Dash-ex" class="label_container">Digital Dash
                <input type="checkbox" id="Digital-Dash-ex" name="extra-options" class="custom_checkbox" data-optionName="Digital Dash">
                <span class="checkmark"></span> </label>
            </div>  
            <div class="optBox">
                <label for="Paddle-Shift-ex" class="label_container">Paddle Shift
                <input type="checkbox" id="Paddle-Shift-ex" name="extra-options" class="custom_checkbox" data-optionName="Paddle Shift">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Heated-Seats-ex" class="label_container">Heated Seats
                <input type="checkbox" id="Heated-Seats-ex" name="extra-options" class="custom_checkbox" data-optionName="Heated Seats">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Seats-ex" class="label_container">Power Seats
                <input type="checkbox" id="Power-Seats-ex" name="extra-options" class="custom_checkbox" data-optionName="Power Seats">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Media-Nav-System-ex" class="label_container">Media & Navigation System
                <input type="checkbox" id="Media-Nav-System-ex" name="extra-options" class="custom_checkbox" data-optionName="Media & Navigation System">
                <span class="checkmark"></span> </label>
            </div> 
            <div class="optBox">
                <label for="Mod-Sound-System-ex" class="label_container">Modern Sound System
                <input type="checkbox" id="Mod-Sound-System-ex" name="extra-options" class="custom_checkbox" data-optionName="Modern Sound System">
                <span class="checkmark"></span> </label>
            </div> 
            
        </div>                              
                                                        </div>                                  
                                                    </div>
                                                </div>
                                                
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse7" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fa fa-car"></i>Packages</a> </h4>
                                                    </div>
                                                    <div id="collapse7" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <h3 class="contant-hed">Ask us about our Spy Package.</h3>
                                                            <!--div class="power-train-data"> 
                                                                <img src="/wp-content/themes/Avada/images/1-1.jpg">
                                                                <ul>
                                                                    <li><i class="fa fa-angle-right"></i>Rear View Camera</li>
                                                                    <li><i class="fa fa-angle-right"></i>TrackApps™</li>
                                                                    <li><i class="fa fa-angle-right"></i>LED Headlamps with LED Signature Lighting</li>
                                                                    <li><i class="fa fa-angle-right"></i>Intelligent Access with Push Button Start</li>
                                                                    <li><i class="fa fa-angle-right"></i>Dual Bright Exhaust with Rolled Tips </li>
                                                                </ul>
                                                                <span>Included</span> 
                                                            </div-->
                                                        </div>
                                                    </div>
                                                </div>