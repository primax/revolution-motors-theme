<div class="panel panel-default">
  <div class="panel-heading open">
    <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="panel-title expand">
      <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
      <a href="javascript:void(0);"><i class="fa fa-paint-brush"></i>Paint & Exterior</a> </h4>
  </div>
  <div id="collapse1" class="panel-collapse collapse in">
    <div class="tool-bar">
      <div class="tab-content">
        <div class="tab-pane active" id="body-color" data-folder="retro-cool">
          <div class="bodyColor8" id="tool_color_type">
            <h3 class="heading-color">Choose Color Type </h3>
            <div class="colorBox">
              <label title="stock Color" for="stockColor" class="label_container">Matte Paint
                <input type="radio" id="stockColor" checked="" name="color-8" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>
            <div class="colorBox">
              <label title="Gloss Color" for="glossColor" class="label_container"> Gloss Paint
                <input type="radio" id="glossColor" name="color-8" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>
          </div>
          <div class="bodyColor" id="tool_paint">
            <h3 class="heading-color">Matte Paint</h3>
            <div class="colorBox">
              <input type="radio" id="color01" name="color" checked="checked"  data-color="black.jpg" data-colorName="Shadow black" data-priceTag="0">
              <label title="black" for="color01"> <i class="Shadow-Black"></i> </label>
              <p class="color-title">Shadow Black</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color03" name="color" data-color="white.jpg" data-colorName="Oxford white" data-priceTag="0">
              <label title="white" for="color03"> <i class="Shadow-white"></i> </label>
              <p class="color-title">Oxford white</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color07" name="color" data-color="silver.jpg" data-colorName="Ingot Silver" data-priceTag="0">
              <label title="silver" for="color07"> <i class="Shadow-silver"></i> </label>
              <p class="color-title">Ingot Silver</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color06" name="color" data-color="red.jpg" data-colorName="Red" data-priceTag="0">
              <label title="red" for="color06"> <i class="Shadow-red"></i> </label>
              <p class="color-title"> Red</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color08" name="color" data-color="yellow.jpg" data-colorName="Light Yellow" data-priceTag="0">
              <label title="yellow" for="color08"> <i class="Shadow-yellow"></i> </label>
              <p class="color-title">Light Yellow</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color04" name="color" data-color="light-blue.jpg" data-colorName="Light Blue" data-priceTag="0">
              <label title="light blue" for="color04"> <i class="Shadow-blue"></i> </label>
              <p class="color-title">Light Blue</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color09" name="color" data-color="dark-blue.jpg" data-colorName="Dark Blue" data-priceTag="0">
              <label title="navy blue" for="color09"> <i class="Shadow-darkblue"></i> </label>
              <p class="color-title">Dark Blue</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color02" name="color" data-color="dark-green.jpg" data-colorName="Dark Green" data-priceTag="0">
              <label title="dark green" for="color02"> <i class="Shadow-green"></i> </label>
              <p class="color-title">Dark Green</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color055" name="color" data-color="light-green.jpg" data-colorName="Light Green" data-priceTag="0">
              <label title="Bright green" for="color055"> <i class="Shadow-light-green"></i> </label>
              <p class="color-title">Light Green</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color066" name="color" data-color="moroon.jpg" data-colorName="Maroon" data-priceTag="450">
              <label title="moroon" for="color066"> <i class="Shadow-moroon"></i> </label>
              <p class="color-title">Maroon<br>
                <small class="price-tag"><!--Add $ 450--></small></p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color10" name="color" data-color="orange.jpg" data-colorName="Orange" data-priceTag="595">
              <label title="Orange" for="color10"> <i class="Shadow-orange"></i> </label>
              <p class="color-title">Orange<br>
                <small class="price-tag"><!--Add $ 595--></small></p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color05" name="color" data-color="dark-grey.jpg" data-colorName="Dark Grey" data-priceTag="395">
              <label title="dark grey" for="color05"> <i class="Shadow-grey"></i> </label>
              <p class="color-title">Dark Grey<br>
                <small class="price-tag"><!--Add $ 395--></small></p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color099" name="color" data-color="cream.jpg" data-colorName="Smooth Cream" data-priceTag="470">
              <label title="Cream" for="color099"> <i class="Shadow-cream"></i> </label>
              <p class="color-title">Smooth Cream<br>
                <small class="price-tag"><!--Add $ 470--></small></p>
            </div>
          </div>
          <div class="bodyColor6" id="tool_paint_gloss">
            <h3 class="heading-color">Gloss Paint </h3>
            <div class="colorBox">
              <input type="radio" id="color31" name="color" checked="checked" data-color="gloss-black.jpg" data-colorName="Gloss black" data-priceTag="0">
              <label title="black" for="color31"> <i class="Shadow-Black"></i> </label>
              <p class="color-title">Gloss Black</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color33" name="color" data-color="gloss-white.jpg" data-colorName="Gloss white" data-priceTag="0">
              <label title="white" for="color33"> <i class="Shadow-white"></i> </label>
              <p  class="color-title">Gloss white</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color37" name="color" data-color="gloss-silver.jpg" data-colorName="Gloss Silver" data-priceTag="0">
              <label title="silver" for="color37"> <i class="Shadow-silver"></i> </label>
              <p class="color-title">Gloss Silver</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color36" name="color" data-color="gloss-red.jpg" data-colorName="Gloss Red" data-priceTag="0">
              <label title="red" for="color36"> <i class="Shadow-red"></i> </label>
              <p class="color-title">Gloss Red</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color38" name="color" data-color="gloss-yellow.jpg" data-colorName="Gloss Yellow" data-priceTag="0">
              <label title="yellow" for="color38"> <i class="Shadow-yellow"></i> </label>
              <p class="color-title">Gloss Yellow</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color34" name="color" data-color="gloss-light-blue.jpg" data-colorName="Gloss Light Blue" data-priceTag="0">
              <label title="light blue" for="color34"> <i class="Shadow-blue"></i> </label>
              <p class="color-title">Gloss Light Blue</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color39" name="color" data-color="gloss-dark-blue.jpg" data-colorName="Gloss Dark Blue" data-priceTag="0">
              <label title="navy blue" for="color39"> <i class="Shadow-darkblue"></i> </label>
              <p class="color-title">Gloss Dark Blue</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color32" name="color" data-color="gloss-dark-green.jpg" data-colorName="Gloss Dark Green" data-priceTag="0">
              <label title="dark green" for="color32"> <i class="Shadow-green"></i> </label>
              <p class="color-title">Gloss Dark Green</p>
            </div>
            <div class="colorBox">

              <input type="radio" id="color155" name="color" data-color="gloss-light-green.jpg" data-colorName="Gloss Light Green" data-priceTag="0">
              <label title="Bright green" for="color155"> <i class="Shadow-light-green"></i> </label>
              <p class="color-title">Gloss Light Green</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color166" name="color" data-color="gloss-moroon.jpg" data-colorName="Gloss Maroon" data-priceTag="230">
              <label title="moroon" for="color166"> <i class="Shadow-moroon"></i> </label>
              <p class="color-title">Gloss Maroon<br>
                <small class="price-tag"><!--Add $ 230--></small></p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color115" name="color" data-color="gloss-orange.jpg" data-colorName="Gloss Orange" data-priceTag="515">
              <label title="Orange" for="color115"> <i class="Shadow-orange"></i> </label>
              <p class="color-title">Gloss Orange<br>
                <small class="price-tag"><!--Add $ 515--></small></p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color405" name="color" data-color="gloss-dark-grey.jpg" data-colorName="Gloss Dark Grey" data-priceTag="395">
              <label title="dark grey" for="color405"> <i class="Shadow-grey"></i> </label>
              <p class="color-title">Gloss Grey<br>
                <small class="price-tag"><!--Add $ 395--></small></p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color799" name="color" data-color="gloss-cream.jpg" data-colorName="Gloss Cream" data-priceTag="570">
              <label title="Cream" for="color799"> <i class="Shadow-cream"></i> </label>
              <p class="color-title">Gloss Cream<br>
                <small class="price-tag"><!--Add $ 570--></small></p>
            </div>
          </div>
          <p class="colorNote">We can paint the vehicle in any color</p>
           
           
          <div class="bodyColor4" id="tool_tire">
            <h3 class="heading-color">Customize Wheels</h3>
            <div class="colorBox">
              <label title="Stock Tire" for="stock" class="label_container">Original Wheels
                <input type="radio" id="stock" checked="checked" name="color-4" data-color="transparent.png" data-wheelsName="Original Wheels" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>
            <div class="colorBox">
              <label title="Modern Tire" for="modern" class="label_container">Modern Chrome Wheels
                <input type="radio" id="modern" name="color-4" data-color="mod-tire.png" data-wheelsName="Modern Chrome Wheels" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>
            <div class="colorBox" id="checkBlack">
              <label title="Black Tire" for="mod-black" class="label_container">Modern Black Wheels
                <input type="radio" id="mod-black" name="color-4" data-color="mod-black-tire.png" data-wheelsName="Modern Black Wheels" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>
          </div>
          <div class="bodyColor2" id="tool_rim">
            <h3 class="heading-color">Wheel color</h3>
            <div class="colorBox">
              <input type="radio" id="color101" name="color-2" checked="checked" data-color="rim-silver.png" data-wheelsColor="Chrome">
              <label title="Silver" for="color101"> <i class="Shadow-silver"></i> </label>
              <p class="color-title">Chrome</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color102" name="color-2" data-color="rim-dark-grey.png" data-wheelsColor="Dark Grey">
              <label title="Dark-grey" for="color102"> <i class="Shadow-grey"></i> </label>
              <p class="color-title">Dark Grey</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color103" name="color-2" data-color="rim-black.png" data-wheelsColor="Shadow Black">
              <label title="Black" for="color103"> <i class="Shadow-Black"></i> </label>
              <p class="color-title">Shadow Black</p>
            </div>
         <!--   <div class="colorBox">
              <input type="radio" id="color104" name="color-2"  data-color="rim-moroon.png" data-wheelsColor="Maroon">
              <label title="moroon" for="color104"> <i class="Shadow-moroon"></i> </label>
              <p class="color-title">Maroon</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color105" name="color-2" data-color="rim-dark-blue.png" data-wheelsColor="Dark Blue">
              <label title="dark-blue" for="color105"> <i class="Shadow-darkblue"></i> </label>
              <p class="color-title">Dark Blue</p>
            </div>-->
          </div>
           
          <div class="bodyColor5" id="tool_stripe">
            <h3 class="heading-color">Racing stripes</h3>
            <div class="colorBox">
              <input type="radio" id="color913" name="color-5" checked="checked" data-color="transparent.png" data-stripeColor="No Stripe">
              <label title="None" for="color913"> <i class="Shadow-white"></i> </label>
              <p class="color-title">No Stripe</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color918" name="color-5" data-color="stripe-black.png" data-stripeColor="Shadow Black">
              <label title="Shadow Black" for="color918"> <i class="Shadow-Black"></i> </label>
              <p class="color-title">Shadow Black</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color917" name="color-5" data-color="stripe-red.png" data-stripeColor="Red">
              <label title="Red" for="color917"> <i class="Shadow-red"></i> </label>
              <p class="color-title">Red</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color916" name="color-5" data-color="stripe-dark-green.png" data-stripeColor="Dark green">
              <label title="Dark Green" for="color916"> <i class="Shadow-green"></i> </label>
              <p class="color-title">Dark green</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color915" name="color-5" data-color="stripe-light-blue.png" data-stripeColor="Light Blue">
              <label title="Light Blue" for="color915"> <i class="Shadow-blue"></i> </label>
              <p class="color-title">Light Blue</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color914" name="color-5" data-color="stripe-orange.png" data-stripeColor="Light Orange">
              <label title="Orange" for="color914"> <i class="Shadow-orange"></i> </label>
              <p class="color-title">Light Orange</p>
            </div>
            <div class="colorBox">
              <input type="radio" id="color953" name="color-5" data-color="stripe-silver.png" data-stripeColor="Ingot silver">
              <label title="Ingot silver" for="color953"> <i class="Shadow-silver"></i> </label>
              <p class="color-title">Ingot silver</p>
            </div>
          </div>
          <div class="bodyColor7" id="tool_spoiler">
            <h3 class="heading-color">Customize Front Spoiler</h3>
            <div class="colorBox">
              <label title="No Spoiler" for="noSpoiler" class="label_container">No Spoiler
                <input type="radio" id="noSpoiler" checked="checked" name="color-7" data-color="transparent.png" data-spoiler="No Spoiler" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>
            <div class="colorBox">
              <label title="Add Spolier" for="addSpoiler" class="label_container"> Add Spoiler
                <input type="radio" id="addSpoiler" name="color-7" data-color="spoiler.png" data-spoiler="Add Spoiler" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>
          </div>
          <div class="bodyColor12" id="tool_hood_scoop">
            <h3 class="heading-color">Customize Hood Scoops</h3>
            <div class="colorBox">
              <label title="No hood Scoop" for="noHood" class="label_container"> No Hood Scoop
                <input type="radio" id="noHood" checked="checked" name="color-12" data-color="transparent.png" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>
            <div class="colorBox">
              <label title="Hood Scoop" for="hood" class="label_container"> Add Hood Scoop
                <input type="radio" id="hood" name="color-12" data-color="hood-scoope.png" class="custom_radio">
                <span class="checkmark"></span> </label>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="panel-title expand">
        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
        <a href="javascript:void(0);"><i class="fa fa-car-battery"></i>Powertrain</a> </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
            <h3 class="contant-hed">Engine/Transmissions</h3>
            <div class="option-view"> 
                <img src="/wp-content/themes/Avada/images/options/425HP-Auto.jpg"> 
            </div> 
                                                            
        <div class="dataRow">
            <h3 class="heading-options">Modern Performance</h3>
            <div class="optBox">
                <label title="435 Horsepower Auto" for="425HP-Auto" class="label_container">435 Horsepower Coyote Engine with a 4R70w Automatic Transmission <strong></strong> 
                <input type="radio" id="425HP-Auto" checked="checked" name="Engine-and-Trans" class="custom_radio" data-elementName="435 Horsepower Coyote Engine with a 4R70w Automatic Transmission" data-pricetag="">
                <span class="checkmark"></span> </label>
            </div>
            <!-- <div class="optBox">
                <label title="430 Horsepower Manual" for="430HP-Man" class="label_container">430 Horsepower LS3 Engine with 6 Speed Manual Transmission <strong>$42,678.00</strong> 
                <input type="radio" id="430HP-Man" checked="checked" name="Engine-and-Trans" class="custom_radio" data-elementName="430 Horsepower LS3 Engine with 6 Speed Manual Transmission" data-pricetag="42678">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label title="430 Horsepower Automatic" for="430HP-Auto" class="label_container">430 Horsepower LS3 Engine with 4L65E Automatic Transmission <strong>$42,972.30</strong> 
                <input type="radio" id="430HP-Auto" name="Engine-and-Trans" class="custom_radio" data-pricetag="42972.30" data-elementName="430 Horsepower LS3 Engine with 4L65E Automatic Transmission">
                <span class="checkmark"></span> </label>
            </div> -->
        </div>
        
        <div class="dataRow">
            <h3 class="heading-options">High Performance</h3>
            <div class="optBox">
                <label title="435 Horsepower Manual" for="435HP-Man" class="label_container">435 Horsepower Coyote Engine with a 6 Speed Manual Transmission <strong></strong> 
                <input type="radio" id="435HP-Man" name="Engine-and-Trans" class="custom_radio" data-elementName="435 Horsepower Coyote Engine with a 6 Speed Manual Transmission" data-pricetag="">
                <span class="checkmark"></span> </label>
            </div>
            <!-- <div class="optBox">
                <label title="556 Horsepower Manual" for="556HP-Man" class="label_container">556 Horsepower LSA Supercharged Engine with 6 Speed Manual Transmission <strong>$50,471.55</strong> 
                <input type="radio" id="556HP-Man" name="Engine-and-Trans" class="custom_radio" data-pricetag="50471.55" data-elementName="556 Horsepower LSA Supercharged Engine with 6 Speed Manual Transmission">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label title="556 Horsepower Automatic" for="556HP-Auto" class="label_container">556 Horsepower LSA Supercharged Engine with 4L75E Automatic Transmission <strong>$50,876.55</strong> 
                <input type="radio" id="556HP-Auto" name="Engine-and-Trans" class="custom_radio" data-pricetag="50876.55" data-elementName="556 Horsepower LSA Supercharged Engine with 4L75E Automatic Transmission">
                <span class="checkmark"></span> </label>
            </div> -->
        </div>
        
        <div class="dataRow">
            <h3 class="heading-options">Competition Power</h3>
            <div class="optBox">
                <label title="785 Horsepower Auto" for="785HP-Auto" class="label_container">785 Horsepower Edelbrock Supercharged Coyote Engine with a 4r70 Transmission<strong></strong> 
                <input type="radio" id="785HP-Auto" name="Engine-and-Trans" class="custom_radio" data-elementName="785 Horsepower Edelbrock Supercharged Coyote Engine with a 4r70 Transmission" data-pricetag="">
                <span class="checkmark"></span> </label>
            </div>
            <!-- <div class="optBox">
                <label title="815 Horsepower Automatic" for="815HP-Auto" class="label_container">815 Horsepower Edelbrock LTI Supercharged Engine with 8L90E Automatic Transmission <strong>$63,392.40</strong> 
                <input type="radio" id="815HP-Auto" name="Engine-and-Trans" class="custom_radio" data-pricetag="63392.40" data-elementName="815 Horsepower Edelbrock LTI Supercharged Engine with 8L90E Automatic Transmission">
                <span class="checkmark"></span> </label>
            </div>  -->
        </div> 
                                                                    
                                                                    
                                                        </div>
                                                    </div>
                                                </div> 
<div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fas fa-car-crash"></i>Brakes</a> </h4>
                                                    </div>
                                                    <div id="collapse3" class="panel-collapse collapse">
                                                        <div class="panel-body"> 
                                                        <div class="option-view">
															<img src="/wp-content/themes/Avada/images/options/DB-4piston.jpg"> 
                                                        </div> 
                                                        <h3 class="heading-options">Select Brake type</h3>
		<div class="dataRow"> 
            <div class="optBox">
                <label title="Disc Brake 4 Piston" for="DB-4piston" class="label_container">Front & Rear Power Disc Brake System with four wheel Wilwood 4 Piston Calipers and Drilled & Slotted Rotors <small>$</small><strong class="DB-Price"></strong> 
                <input type="radio" id="DB-4piston" checked="checked" name="brakes-type" class="custom_radio" data-elementName="Front & Rear Power Disc Brake System with four wheel Wilwood 4 Piston Calipers and Drilled & Slotted Rotors">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label title="Disc Brake 6 Piston" for="DB-6piston" class="label_container">Front & Rear Power Disc Brake System with Wilwood 6 Piston Front/4 Piston Rear Calipers and Drilled & Slotted Rotors <small>$</small><strong class="DB-Price"></strong>
                <input type="radio" id="DB-6piston" name="brakes-type" class="custom_radio" data-elementName="Front & Rear Power Disc Brake System with Wilwood 6 Piston Front/4 Piston Rear Calipers and Drilled & Slotted Rotors">
                <span class="checkmark"></span> </label>
            </div>
        </div>
        
 
                                                                    
                                                                    
                                                        </div>
                                                    </div>
                                                </div> 
<div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fas fa-couch"></i>Interior</a> </h4>
                                                    </div>
                                                    <div id="collapse4" class="panel-collapse collapse">
                                                        <div class="panel-body"> 
                                                            <div class="option-view">
                                                                <img src="/wp-content/themes/Avada/images/options/Stock-Original-inter.jpg"> 
                                                            </div>
                                                            <h3 class="heading-options">Select Interior type</h3>
		<div class="dataRow"> 
            <div class="optBox">
                <label for="Stock-Original-inter" class="label_container">Stock Original Interior <strong>$11,475.00</strong>
                <input type="radio" id="Stock-Original-inter" checked="checked" name="interior-type" class="custom_radio" data-elementName="Stock Original Interior" data-pricetag="11475">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Performance-Style-c-inter" class="label_container">Performance Style Custom Interior <strong>$20,250.00</strong>
                <input type="radio" id="Performance-Style-c-inter" name="interior-type" class="custom_radio" data-elementName="Performance Style Custom Interior" data-pricetag="20250">
                <span class="checkmark"></span> </label>
            </div> 
            <div class="optBox">
                <label for="Custom-Luxury-inter" class="label_container">Custom Luxury Interior - <strong>Call for pricing</strong> on Custom Luxury Interiors
                <input type="radio" id="Custom-Luxury-inter" name="interior-type" class="custom_radio" data-elementName="Custom Luxury Interior" data-pricetag="Call for pricing">
                <span class="checkmark"></span> </label>
            </div>
        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                   
                                                
<div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse6B" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus"></i>Extras</a> </h4>
                                                    </div>
                                                    <div id="collapse6B" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="option-view--">
                                                                <img src="/wp-content/themes/Avada/images/options/extra-options.jpg" style="max-width:100%"> 
                                                            </div>
                                                            <h3 class="heading-options">Select Extra Options</h3>
		<div class="dataRow"> 
            <div class="optBox">
                <label for="Climate-Control-ex" class="label_container">Climate Control
                <input type="checkbox" id="Climate-Control-ex" name="extra-options" class="custom_checkbox" data-optionName="Climate Control">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Windows-ex" class="label_container">Power Windows
                <input type="checkbox" id="Power-Windows-ex" name="extra-options" class="custom_checkbox" data-optionName="Power Windows">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Locks-ex" class="label_container">Keyless Entry Power Locks
                <input type="checkbox" id="Power-Locks-ex" name="extra-options" class="custom_checkbox" data-optionName="Keyless Entry Power Locks">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Locks-ss-ex" class="label_container">Keyless Entry Power Locks with Security System
                <input type="checkbox" id="Power-Locks-ss-ex" name="extra-options" class="custom_checkbox" data-optionName="Keyless Entry Power Locks with Security System">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Auto-Trunk-ex" class="label_container">Automatic Trunk Opening System
                <input type="checkbox" id="Auto-Trunk-ex" name="extra-options" class="custom_checkbox" data-optionName="Automatic Trunk Opening System">
                <span class="checkmark"></span> </label>
            </div> 
            <div class="optBox">
                <label for="Digital-Dash-ex" class="label_container">Digital Dash
                <input type="checkbox" id="Digital-Dash-ex" name="extra-options" class="custom_checkbox" data-optionName="Digital Dash">
                <span class="checkmark"></span> </label>
            </div>  
            <div class="optBox">
                <label for="Paddle-Shift-ex" class="label_container">Paddle Shift
                <input type="checkbox" id="Paddle-Shift-ex" name="extra-options" class="custom_checkbox" data-optionName="Paddle Shift">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Heated-Seats-ex" class="label_container">Heated Seats
                <input type="checkbox" id="Heated-Seats-ex" name="extra-options" class="custom_checkbox" data-optionName="Heated Seats">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Power-Seats-ex" class="label_container">Power Seats
                <input type="checkbox" id="Power-Seats-ex" name="extra-options" class="custom_checkbox" data-optionName="Power Seats">
                <span class="checkmark"></span> </label>
            </div>
            <div class="optBox">
                <label for="Media-Nav-System-ex" class="label_container">Media & Navigation System
                <input type="checkbox" id="Media-Nav-System-ex" name="extra-options" class="custom_checkbox" data-optionName="Media & Navigation System">
                <span class="checkmark"></span> </label>
            </div> 
            <div class="optBox">
                <label for="Mod-Sound-System-ex" class="label_container">Modern Sound System
                <input type="checkbox" id="Mod-Sound-System-ex" name="extra-options" class="custom_checkbox" data-optionName="Modern Sound System">
                <span class="checkmark"></span> </label>
            </div> 
            
        </div>                              
                                                        </div>                                  
                                                    </div>
                                                </div>
                                                
<div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse7B" class="panel-title expand">
                                                        <div class="right-arrow pull-right"><i class="fa fa-chevron-down"></i></div>
                                                        <a href="javascript:void(0);"><i class="fa fa-car"></i>Packages</a> </h4>
                                                    </div>
                                                    <div id="collapse7B" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <h3 class="contant-hed">Ask us about our Spy Package.</h3>
                                                            <!--div class="power-train-data"> 
                                                                <img src="/wp-content/themes/Avada/images/1-1.jpg">
                                                                <ul>
                                                                    <li><i class="fa fa-angle-right"></i>Rear View Camera</li>
                                                                    <li><i class="fa fa-angle-right"></i>TrackApps™</li>
                                                                    <li><i class="fa fa-angle-right"></i>LED Headlamps with LED Signature Lighting</li>
                                                                    <li><i class="fa fa-angle-right"></i>Intelligent Access with Push Button Start</li>
                                                                    <li><i class="fa fa-angle-right"></i>Dual Bright Exhaust with Rolled Tips </li>
                                                                </ul>
                                                                <span>Included</span> 
                                                            </div-->
                                                        </div>
                                                    </div>
                                                </div>